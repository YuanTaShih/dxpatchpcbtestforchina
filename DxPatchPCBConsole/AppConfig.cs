﻿namespace DxPatchPCBConsole
{
    public static class AppConfig
    {
        public static string DateTimeFormat { get; } = "yyyy-MM-dd hh:mm:ss.fff";
        public static int PatchSampleRate { get; } = 500;
        public static double PatchEcgResolution { get; } = 0.005;
        public static string PPIDPlaceholder { get; } = "CH XX ID XXXXX";
        public static string OutputDirectory { get; set; } = @"c:\temp";  
    }
}
