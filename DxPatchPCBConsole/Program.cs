﻿using System;
using System.IO;
using DxPatchPCBTestApp;
using System.Drawing;
using System.Diagnostics;

namespace DxPatchPCBConsole
{
    class Program
    {
        private static StringWriter m_swConsoleOut = new StringWriter();
        private const string OUTPUT_DIR = @"C:\temp\rawbulk";

        static void Main(string[] args)
        {
            TestRawFiles(@"C:\Temp\8zp7_raw\all_dirs.txt", OUTPUT_DIR);

            //string rawFile = @"C:\Temp\rawtests\data.raw";

            //SignalTestReport report = CreateReport(rawFile, Path.GetDirectoryName(rawFile), "CH 02 ID 11000");
            //report.SavePDF(rawFile + ".pdf");
            //Process.Start(rawFile + ".pdf");

            Console.ReadLine();
        }

        private static void TestRawFiles(string testFileScript, string outputDir)
        {
            //Console.SetOut(m_swConsoleOut);

            string[] dirs = GetTestDirPaths(testFileScript);

            foreach (string strPath in dirs)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(strPath);
                string[] rawFiles = Directory.GetFiles(strPath, "*.raw");

                if (rawFiles != null && rawFiles.Length >= 1) {
                    Console.WriteLine(dirInfo.Name + " " + TestRawFile(rawFiles[0], outputDir));
                    if (OUTPUT_DIR.Trim() != "" && !Directory.Exists(OUTPUT_DIR)) Directory.CreateDirectory(OUTPUT_DIR);
                    if (Directory.Exists(OUTPUT_DIR)) {
                        TestRawFile(rawFiles[0], OUTPUT_DIR);
                    }

                }
            }
            string stdOutText = m_swConsoleOut.ToString();

            //Console.Clear();
            //StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            //standardOutput.AutoFlush = true;
            //Console.SetOut(standardOutput);

            Console.WriteLine(stdOutText);
        }

        private static bool TestRawFile(string rawFilePath, string outputDir)
        {
            if (outputDir == "") outputDir = @"C:\temp\rawtests";
            string testscriptPath = Path.GetDirectoryName(rawFilePath) + @"\sig-test-script.xml";
            
            //Run the tester to get the results
            SignalTester tester = new SignalTester(
                testscriptPath,
                rawFilePath,
                AppConfig.PatchSampleRate,
                AppConfig.PatchEcgResolution
            );

            //Console.WriteLine("Testing RAW file: " + rawFilePath);
            SignalTestResultHolder resultWrapper = tester.RunTests();

            var signalPoints = DataTools.GetHemoDataAsPoints(rawFilePath, 1, true);

            string dt = DateTime.Now.ToString("yyyyMMdd_HHmmss_fff_");
            string chartFileName = outputDir + "\\" + dt  + "chart_" + resultWrapper.IsPass().ToString();
            string reportFileName = outputDir + "\\" + dt + "report_" + resultWrapper.IsPass().ToString();

            SignalTestReport report = new SignalTestReport(
                resultWrapper, 
                DateTime.Now, 
                "TESTPPID", 
                DataTools.GetHemoFirmwareVersion(rawFilePath), 
                tester.ReportData, 
                signalPoints, 
                rawFilePath,
                testscriptPath
            );

            report.AppendToBody(tester.GetReportText());
            reportFileName += ".pdf";
            report.SavePDF(reportFileName);
            //System.Diagnostics.Process.Start(reportFileName);

            return resultWrapper.IsPass();
        }


        private static string[] GetTestDirPaths()
        {
            string[] testDirs = new string[] {
                @"C:\dev\8ZP7 PCB Test App Report\DxPatch-CH_02_ID_11011_20151102-042822\",
                @"C:\dev\8ZP7 PCB Test App Report\DxPatch-CH_02_ID_11022_20151102-044856\",
                @"C:\dev\8ZP7 PCB Test App Report\DxPatch-CH_02_ID_11023_20151102-044506\",
                @"C:\dev\8ZP7 PCB Test App Report\DxPatch-CH_02_ID_11026_20151102-033457\"
            };

            return testDirs;
        }

        private static string[] GetTestDirPaths(string listFilePath)
        {
            string[] paths = File.ReadAllLines(listFilePath);

            return paths;
        }

        private static SignalTestReport CreateReport(string rawFilePath, string outputDir, string ppid = "TESTPPID")
        {
            if (outputDir == "") outputDir = @"C:\temp\rawtests";
            string testscriptFilePath = Path.GetDirectoryName(rawFilePath) + @"\sig-test-script.xml";

            //Run the tester to get the results
            SignalTester tester = new SignalTester(
                testscriptFilePath,
                rawFilePath,
                AppConfig.PatchSampleRate,
                AppConfig.PatchEcgResolution
            );

            SignalTestResultHolder resultHolder = tester.RunTests();
            var signalPoints = DataTools.GetHemoDataAsPoints(rawFilePath, 1, true);

            SignalTestReport report = new SignalTestReport(
                resultHolder,
                DateTime.Now,
                ppid,
                DataTools.GetHemoFirmwareVersion(rawFilePath),
                tester.ReportData,
                signalPoints,
                rawFilePath,
                testscriptFilePath
            );

            return report;
        }
    }
}
