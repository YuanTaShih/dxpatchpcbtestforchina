﻿using System;
using System.Runtime.InteropServices;    // For StructLayout, DllImport

namespace WhaleTeqSECG_SDK
{
    public enum OutputFunction
    {
        Output_Off  = 0,
        Output_Sine,
        Output_Triangle,
        Output_Square,
        Output_RectanglePulse,
        Output_TrianglePulse,
        Output_Exponential,
        Output_ECG2_27,
        Output_IEC227W,
        Output_IEC251W,
        Output_JJG1041,
        Output_JJG1041_HR,
    };

    enum PacingPulse_E
    {
        SinglePulse = 0,
        DoublePulse_150ms,
        DoublePulse_250ms
    };

    public enum OutputLead_E 
    {
		Lead_RA		= 0,	//(R)
		Lead_LA,			//(L)
		Lead_LL,			//(F)
		Lead_V1,
		Lead_V2,
		Lead_V3,
		Lead_V4,
		Lead_V5,
		Lead_V6
	};

    enum BaselineReset_E 
    {
		Baseline_Off	= 0,
		Baseline_50Hz,
		Baseline_60Hz,
		Baseline_80Hz,
		Baseline_100Hz
	};

    enum ECG2_27_NoiseFreq_E 
    {
		Noise_None	= 0,
		Noise_50Hz,
		Noise_60Hz
	};

    enum SpecialWave227{
        IEC227W_3A_A1 = 0,
        IEC227W_3B_A2,
        IEC227W_3C_A3,
        IEC227W_3D_A4,
        IEC227W_4A_B1,
        IEC227W_4A_B1x2,
        IEC227W_4A_B1d2,
        IEC227W_4B_B2,
        IEC227W_4B_B2x2,
        IEC227W_4B_B2d2,

    };
    enum SpecialWave251{
        IEC251W_1 = 0,
        IEC251W_2,
        IEC251W_3,
        IEC251W_4,
        IEC251W_6,
        IEC251W_10,
    };

   

    class SECG_SDK
    {
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool InitSECG();
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetCTS();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ResetSECG();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CloseSECG();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.LPStr)]
        public static extern string GetSerialNo();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RegisterTimeAmpCB(IntPtr cb);
        //Parameter - cb:
        //  function pointer of delegate from "Marshal.GetFunctionPointerForDelegate(delegate)"
        //      delegate void TimeAmplitudeCBDelegate(double time, double amp);
        //      Parameter - time
        //	        unit: secondG
        //	        time from the output signal changed(function, amplitude, frequency ...)
        //      Parameter - amp
        //	        unit: microvolt
        //About 0.006 second(6ms) interval between each callback be triggered

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RegisterPacingTimeAmpCB(IntPtr cb);

         //Return value:
        // 0:  succeed
        // -1: Mainfunction not set to IEC227W
        // Call after set main func to Output_IEC227w
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetSpecialWave227(SpecialWave227 spec);
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern SpecialWave227 GetSpecialWave227();
        //Return value:
        // 0:  succeed
        // -1: Mainfunction not set to IEC251W
        // Call after set main func to Output_IEC251w
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetSpecialWave251(SpecialWave251 spec);
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern SpecialWave251 GetSpecialWave251();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetOutputFunc(OutputFunction func);
        //Return value:
        //	0:	succeed
        //	-1:	SECG not be initialized
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern OutputFunction GetOutputFunc();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetAmplitude(double amp);
        //Parameter - amp:
        //	value range: -10 ~ 10
        //	unit: microvolt
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //	-2: value out of range
        //  -3: "Baseline Reset Test" is working
        //Default value after InitSECG(): 1mV
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetAmplitude();
        public const double DefaultAmplitude = 1.0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetFrequency(double freq);
        //Parameter - freq:
        //	value range: 0.05 ~ 500
        //               0.05 ~ 3 when Output as Pulse Waveforms
        //	unit: hertz
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //	-2: value out of range
        //	-3: "Frequency Scan" is working
        //  -4: "Baseline Reset Test" is working
        //Default value after InitSECG(): 1Hz
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetFrequency();
        public const double DefaultFrequency = 1.0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetDCOffset(int setting);
        //Parameter - setting:
        //	value range: -300 ~ 300 - DCOffset Variable mode Off
        //               -1000 ~ 1000 - DCOffset Variable mode On
        //	unit: microvolt(mV)
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //	-2: value out of range
        //Default value after InitSECG(): 0mV
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetDCOffset();
        public const int DefaultDCOffset = 0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetDCOffsetVariableMode(bool OnOff);
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //Default value after InitSECG(): false(Off)
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetDCOffsetVariableMode();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetDCOffsetCommoneMode(bool OnOff);
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //Default value after InitSECG(): false(Off)
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetDCOffsetCommoneMode();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetPulseWidth(double width);
        //Parameter - width:
        //	value range: 2 ~ 300
        //	unit: milisecond(ms)
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //	-2: value out of range
        //Default value after InitSECG(): 100ms
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetPulseWidth();
        public const double DefaultPulseWidth = 100;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetInputImpedanceMode(bool OnOff);
        //Return value:
        //	0: succeed
        //	-1: SECG not be initialized
        //Default value after InitSECG(): true(On - 620k&/4.7nF shorted)
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetInputImpedanceMode();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetPacingPulseMode(PacingPulse_E mode);
        //Return value:
        //	0:	succeed
        //	-1:	SECG not be initialized
        //Default value after InitSECG(): SinglePulse
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern PacingPulse_E GetPacingPulseMode();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetQRSDuration(double time);
        //Parameter - time:
	    //	value range: 5 ~ 120
	    //	unit: milisecond(ms)
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: value out of range
	    //Default value after InitSECG(): 100ms
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetQRSDuration();
        public const double DefaultQRSDuration = 100.0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetTWave(double amp);
	    //Parameter - amp:
	    //	value range: 0 ~ 2.5
	    //	unit: microvolt(mV)
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: value out of range
	    //Default value after InitSECG(): 0.2mV
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetTWave();
        public const double DefaultTWave = 0.2;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetPacingAmplitude(double amp);
	    //Parameter - amp:
	    //	value range: -700 ~ 700
	    //	unit: microvolt(mV)
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: value out of range
	    //Default value after InitSECG(): 0mV
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetPacingAmplitude();
        public const double DefaultPacingAmplitude = 0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetPacingDuration(double time);
	    //Parameter - time:
	    //	value range: 0.1 ~ 2
	    //	unit: milisecond(ms)
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: value out of range
	    //Default value after InitSECG(): 2ms
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetPacingDuration();
        public const double DefaultPacingDuration = 2.0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetPacingRate(uint BPM);
	    //Parameter - BPM:
	    //	value range: 10 ~ 300
	    //	unit: beats per minute(BPM)
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: value out of range
	    //Default value after InitSECG(): 60BPM
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint GetPacingRate();
        public const int DefaultPacingRate = 60;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetOvershootTimeConstant(uint time);
	    //Parameter - time:
	    //	value range: 0 ~ 100
	    //	unit: milisecond(ms)
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: value out of range
	    //Default value after InitSECG(): 0ms
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint GetOvershootTimeConstant();
        public const uint DefaultOvershootTimeConstant = 0;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetOutputLead(OutputLead_E lead, bool OnOff);
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //Default value after InitSECG(): Lead_RA - On; all others - Off;
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetOutputLead(OutputLead_E lead);

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RegisterSyncFreqCB(IntPtr cb);
        //Parameter - cb:
        //  function pointer of delegate from "Marshal.GetFunctionPointerForDelegate(delegate)"
        //      delegate void SyncFreqCBDelegate(double freq);
        //      Parameter - freq
        //	        unit: hertz
        //	        frequency value changed during "Baseline Reset Test" or "Frequency Scanning"


        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetBaselineResetTest(BaselineReset_E mode);        	
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: current main output function not be "Output_Sine"
	    //Default value after InitSECG(): Baseline_Off
	    //"Output Function" must be set to "Output_Sine" before setting as "non - Baseline_Off" value
	    //After setting as "non - Baseline_Off" value, "Amplitude" would be locked to "2.5mV"
	    //and "Frequency" would be locked as "50/60/80/100" Hz.
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern BaselineReset_E GetBaselineResetTest();

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetECG2_27_Noise(ECG2_27_NoiseFreq_E freq, double amp);
	    //Parameter - freq:
	    //	Default value after InitSECG(): Noise_None
	    //Parameter - amp:
	    //	value range: 0.01 ~ 0.2
	    //	unit: microvolt(mV)
	    //	Default value after InitSECG(): 0.1mV
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
        //	-2: current main output function not be "Output_ECG2_27"
	    //	-3: amp out of range
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern ECG2_27_NoiseFreq_E SetECG2_27_Noise();
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetECG2_27_NoiseAmp();
        public const double DefaultECG2_27_NoiseAmp = 0.1;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetDynamicRangeTest(bool OnOff, double amp, double freq);
        //Parameter - amp:
	    //	value range: 0 ~ 5
	    //	unit: microvolt(mV)
	    //	Default value after InitSECG(): 1mV
	    //Parameter - freq:
	    //	value range: 10 ~ 50
	    //	unit: hertz
	    //	Default value after InitSECG(): 40Hz
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
        //	-2: current main output function not be "Output_Square"
	    //	-3: amp out of range
	    //	-4: freq out of range
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetDynamicRangeTest(bool OnOff);
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetDynamicRangeTestAmp();
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetDynamicRangeTestFreq();
        public const double DefaultDynamicRangeTestAmp = 1.0;
        public const double DefaultDynamicRangeTestFreq = 40;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RegisterSyncMainFuncCB(IntPtr cb);
        //Parameter - cb:
        //  function pointer of delegate from "Marshal.GetFunctionPointerForDelegate(delegate)"
        //      delegate void syncMainFuncCBDelegate(OutputFunction func);
        //      Parameter - func
        //	        main function changed during "Frequency Scanning"

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetFreqScanSine(bool OnOff, double startFreq, 
											     double stopFreq, int duration);
        //Parameter - startFreq:
	    //	value range: 0.67 ~ 500
	    //	unit: hertz
	    //	Default value after InitSECG(): 0.67Hz
	    //Parameter - stopFreq:
	    //	value range: 0.05 ~ 500
	    //	unit: hertz
	    //	Default value after InitSECG(): 150Hz
	    //Parameter - duration:
	    //	value range: 10 ~ 180
	    //	unit: milisecond(ms)
	    //	Default value after InitSECG(): 30ms
	    //Return value:
	    //	0: succeed
	    //	-1: SECG not be initialized
	    //	-2: startFreq out of range
	    //	-3: stopFreq out of range	    //	-4: duration out of range
	    //"Output Function" would be set to "Output_Sine" when setting "OnOff" as true,
	    //then "Frequency" would be locked(SetFrequency() return -3) for periodly scanning change.
	    //"Output Function" would be set to "Output_Off" when setting "OnOff" as false.
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetFreqScanSine();
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetFreqScanSineStartFreq();
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double GetFreqScanSineStopFreq();
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetFreqScanSineDuration();
        public const double DefaultScanSineStartFreq = 0.67;
        public const double DefaultScanSineStopFreq = 150;
        public const int DefaultScanSineDuration = 30;

        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
	    public static extern int SetFreqScanECG(bool OnOff);
        //Start Frequency as 3BPM(0.05Hz)
	    //Stop Frequency as 30BPM(0.5Hz)
	    //Duration as 30ms
	    //"Output Function" would be set to "Output_ECG2_27" when setting "OnOff" as true,
	    //then "Frequency" would be locked(SetFrequency() return -3) for periodly scanning change.
	    //"Output Function" would be set to "Output_Off" when setting "OnOff" as false.
        [DllImport(@"WhaleTeqSECG_SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool GetFreqScanECG();
    }
}
