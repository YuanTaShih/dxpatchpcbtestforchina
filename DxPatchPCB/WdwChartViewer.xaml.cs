﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using System.Drawing;

namespace DxPatchPCBTestApp
{
    public partial class WdwChartViewer : Window
    {
        string _rawFilePath;
        int _chanIndex;
        bool _adjustData;
        Chart _chart;     

        public WdwChartViewer(Chart chart)
        {
            InitializeComponent();
            chartImage.Source = ImageHelper.ToImageSource(chart.DrawBitmap());
        }

        public WdwChartViewer(string rawFilePath, int chanIndex, bool adjustData)
        {
            InitializeComponent();
            _rawFilePath = rawFilePath;
            _chanIndex = chanIndex;
            _adjustData = adjustData;
        }

        private Bitmap GetChartBitmap()
        {
            Bitmap chartBitmap = new Bitmap(1,1);

            Console.WriteLine("ci " + _chanIndex + " ad " + _adjustData);
            System.Drawing.Point[] imagePoints = DataTools.GetHemoDataAsPoints(_rawFilePath, _chanIndex, _adjustData);

            if (imagePoints != null && imagePoints.Length >= 1)
            {
                _chart = new Chart(
                    imagePoints,
                    xAxisMinorUnit: AppConfig.PatchSampleRate,
                    xAxisMajorUnit: AppConfig.PatchSampleRate * 10
                );

                chartBitmap = _chart.DrawBitmap();
            }

            return chartBitmap;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TextBlockMessage.Text = "Drawing chart...";

            Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(() => {
                if (!File.Exists(_rawFilePath)) return;
                chartImage.Source = ImageHelper.ToImageSource(GetChartBitmap());
                TextBlockMessage.Text = "";
            }));
        }

        private void MenuItemEditChartRange_Click(object sender, RoutedEventArgs e)
        {
            DialogModifyChart dialogChartRange = new DialogModifyChart(_chart);

            if (dialogChartRange.ShowDialog() == true)
            {
                string[] rangeStartEnd = dialogChartRange.TxtChartRange.Text.Split(new char[] { ':' });
                int start = Int32.Parse(rangeStartEnd[0]);
                int end = Int32.Parse(rangeStartEnd[1]);

                if (start >= 0 && start <= end)
                {
                    _chart.m_dataRangeStart = start;
                    _chart.m_dataRangeEnd = end;
                    chartImage.Source = ImageHelper.ToImageSource(
                        _chart.DrawBitmap()
                    );
                }
            }
        }
    }
}
