﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;

namespace DxPatchPCBTestApp
{
    public class SignalTestResultChart : Chart
    {
        public SignalTestResultChart(Point[] points, SignalTestResultHolder testResult, int chartWidth = 800, int chartHeight = 800, int xAxisMinorUnit = 500, int xAxisMajorUnit = 5000) 
            : base(points, chartWidth, chartHeight, xAxisMinorUnit, xAxisMajorUnit)
        {
             //Add start marker annotation
            Chart.Annotation startMarker = new Chart.Annotation();
            startMarker.Text = new Chart.TextAnnotation {
                Text = "Start\nMarker",
                X = testResult.CandidateFileStartIndex, Y = 0
            };
            startMarker.Line = new Chart.LineAnnotation {
                X1 = testResult.CandidateFileStartIndex, Y1 = 0,
                X2 = testResult.CandidateFileStartIndex, Y2 = this.ChartHeight / 2
            };
            this.Annotations.Add(startMarker);

            //Add signal tests annotation
            for (int i = 0; i < testResult.Results.Length; i++) {

                SignalTestResult sigResult = testResult.Results[i];
                if (sigResult.SampleChunk == null) continue;
                int x = sigResult.SampleChunk.StartIndex;
                string resultText = (i+1).ToString();

                Chart.Annotation ann = new Chart.Annotation {
                    Text = new Chart.TextAnnotation { Text = resultText, X = x, Y = 9 },
                    Line = new Chart.LineAnnotation { X1 = x, Y1 = 0, X2 = x, Y2 = this.ChartHeight - 30 }
                };

                this.Annotations.Add(ann);
            }
        }
    }

    public class Chart
    {
        public Point[] DataPoints { get; private set; }
        private int m_xAxisMinorUnit = 500;
        private int m_xAxisMajorUnit = 1000;
        private int m_yAxisMinorUnit = 20;
        private int m_yAxisMajorUnit = 200;
        private Pen m_axisPen = new Pen(Color.LightGray);
        private Pen m_lineAnnotationPen = new Pen(Color.Blue, 2f);
        
        public int m_dataRangeStart = 0;
        public int m_dataRangeEnd = 0;
        public bool ShowMajorGrid { get; set; } = true;
        public bool ShowMinorGrid { get; set; } = false;
        public int ChartWidth { get; private set; } = 800;
        public int ChartHeight { get; private set; } = 300;

        public List<Annotation> Annotations { get; } = new List<Annotation>();

        public Chart(Point[] points, int chartWidth = 800, int chartHeight = 800, int xAxisMinorUnit = 500, int xAxisMajorUnit = 5000) {
            DataPoints = points;
            m_xAxisMinorUnit = xAxisMinorUnit;
            m_xAxisMajorUnit = xAxisMajorUnit;
            m_dataRangeEnd = points.Length - 1;
            ChartWidth = chartWidth;
            ChartHeight = chartHeight;
            m_lineAnnotationPen.DashPattern = new float[] { 6f, 6f };
        }

        public Bitmap DrawBitmap()
        {
            Bitmap chartBitmap = new Bitmap(ChartWidth, ChartHeight);
            Point[] points = DataPoints.Skip(m_dataRangeStart).Take(m_dataRangeEnd - m_dataRangeStart).ToArray();
            Point[] scaledPoints;

            scaledPoints = ScalePoints(points);

            //Draw the chart components
            using (Graphics graphics = Graphics.FromImage(chartBitmap))
            {
                DrawBackground(graphics, chartBitmap.Width, chartBitmap.Height);
                DrawGrid(graphics);
                DrawAxis(graphics);
                DrawDataSeries(graphics, scaledPoints.ToArray());
                DrawResultsAnnotation(graphics);
            }

            return chartBitmap;
        }

        private void DrawBackground(Graphics g, int width, int height)
        {
            Rectangle ImageSize = new Rectangle(0, 0, width, height);
            g.FillRectangle(Brushes.White, ImageSize);
            g.DrawRectangle(Pens.Gray, 0, 0, width - 1, height - 1);
        }

        private void DrawGrid(Graphics g)
        {
            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            double xAxisMinorUnitScaled = m_xAxisMinorUnit * sf.ScaleFactorX;
            double xAxisMajorUnitScaled = m_xAxisMajorUnit * sf.ScaleFactorX;
            double yAxisMinorUnitScaled = m_yAxisMinorUnit * sf.ScaleFactorY;
            double yAxisMajorUnitScaled = m_yAxisMajorUnit * sf.ScaleFactorY;

            //VERTICAL MINOR lines but don't draw lines if will be too small */
            if (ShowMinorGrid && xAxisMinorUnitScaled >= 5) {
                for (float x = 0; x < ChartWidth; x += (float)Math.Round(xAxisMinorUnitScaled))
                    g.DrawLine(m_axisPen, x, 0, x, ChartHeight);
            }

            //VERTICAL MAJOR lines but don't draw lines if will be too small */
            if (ShowMajorGrid && xAxisMajorUnitScaled >= 5) {
                for (float x = 0; x < ChartWidth; x += (float)Math.Round(xAxisMajorUnitScaled))
                    g.DrawLine(m_axisPen, x, 0, x, ChartHeight);
            }

            //HORIZONTAL MINOR lines but don't draw lines if will be too small */
            if (ShowMinorGrid && yAxisMinorUnitScaled >= 5) {
                for (float y = 0; y < ChartHeight; y += (float)Math.Round(yAxisMinorUnitScaled))
                    g.DrawLine(m_axisPen, 0, y, ChartWidth, y);
            }

            //HORIZONTAL MAJOR lines but don't draw lines if will be too small */
            if (ShowMajorGrid && yAxisMajorUnitScaled >= 5) {
                for (float y = 0; y < ChartHeight; y += (float)Math.Round(yAxisMajorUnitScaled))
                    g.DrawLine(m_axisPen, 0, y, ChartWidth, y);
            }
        }

        private void DrawDataSeries(Graphics g, System.Drawing.Point[] points)
        {
            //Draw the data points and connecting lines
            if (points.Length >= 1) {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                using (Pen dataPen = new Pen(Color.Black, 1)) {
                    g.DrawLines(dataPen, points);
                }
            }
        }

        private void DrawAxis(Graphics g)
        {
            //Add x-axis horizontal line
            int x1 = 0;
            int x2 = ChartWidth;
            int y1 = (int)Math.Round((double)ChartHeight / 2);

            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            g.DrawLine(m_axisPen, x1, y1, x2, y1);

            //Add x-axis vertical tick lines
            double xAxisMajorUnitScaled = m_xAxisMajorUnit * sf.ScaleFactorX;

            //Labels
            for (double i = 0; i * xAxisMajorUnitScaled < ChartWidth; i++)
            {
                double xPos = Math.Round(i * xAxisMajorUnitScaled) - (m_dataRangeStart * sf.ScaleFactorX);

                string xValueLabel = (i * m_xAxisMajorUnit).ToString();

                g.DrawString(
                    xValueLabel,
                    new Font("Arial", 12),
                    Brushes.Black,
                    (float)xPos - 5,
                    ChartHeight - 20
                );
            }
        }

        public void DrawResultsAnnotation(Graphics g)
        {
            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            if (Annotations != null) {
                foreach (Annotation a in Annotations) {

                    g.DrawLine(
                        m_lineAnnotationPen, 
                        a.Line.X1 * sf.ScaleFactorX, 
                        a.Line.Y1, 
                        a.Line.X2 * sf.ScaleFactorX, 
                        a.Line.Y2
                    );

                    if (a.Text.Text != null) {
                        float xScaled = a.Text.X * sf.ScaleFactorX;
                        g.DrawString(a.Text.Text, new Font("Arial", 16), Brushes.Black, new PointF(xScaled, a.Text.Y));
                    }
                }
            }
        }

        public static ScaleFactor CalculateScaleFactors(Point[] dataPoints, float chartWidth, float chartHeight)
        {
            MinMax m = GetMinMax(dataPoints);

            ScaleFactor sf = new ScaleFactor();

            //Calculate x scale factor
            sf.ScaleFactorX = chartWidth / (m.MaxX - m.MinX);

            //Calculate y scale factor
            int yMax = (Math.Abs(m.MinY) > m.MaxY) ? Math.Abs(m.MinY) : m.MaxY;
            sf.ScaleFactorY = (chartHeight / 2) / yMax;

            return sf;
        }

        private System.Drawing.Point[] ScalePoints(Point[] points)
        {
            var scaledPoints = new List<System.Drawing.Point>();

            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            for (int i = 0; i < points.Length; i++)
            {
                int scaledY = (int)ChartHeight / 2 + -1 * (int)Math.Round(points[i].Y * sf.ScaleFactorY);
                int scaledX = (int)Math.Round((points[i].X - m_dataRangeStart) * sf.ScaleFactorX);
                Point modifiedPoint = new Point(scaledX, scaledY);
                scaledPoints.Add(modifiedPoint);
            }

            return scaledPoints.ToArray();
        }

        private void BitmapPointsToCsv(System.Drawing.Point[] points, string csvFilePath)
        {
            try
            {
                using (var sw = new StreamWriter(csvFilePath))
                {
                    foreach (System.Drawing.Point pt in points)
                    {
                        sw.WriteLine(string.Format("{0},{1}", pt.X.ToString(), pt.Y.ToString()));
                        sw.Flush();
                    }
                    Console.WriteLine("Saved bitmap point CSV data: " + csvFilePath + "\n");
                    sw.Close();
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void PointsToCSV(string csvFilePath) {

            try
            {
                using (var sw = new StreamWriter(csvFilePath))
                {
                    foreach (System.Drawing.Point pt in DataPoints)
                    {
                        sw.WriteLine(string.Format("{0},{1}", pt.X.ToString(), pt.Y.ToString()));
                        sw.Flush();
                    }
                    Console.WriteLine("Saved point CSV data: " + csvFilePath + "\n");
                    sw.Close();
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void SaveChart(string filePath)
        {
            //AppConfig.workingDirectoryPath + @"\secg_chart.png"
            try {
                this.DrawBitmap().Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
                Console.WriteLine("\nBitmap chart saved to " + filePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR SAVING BITMAP CHART to " + filePath + "\n" + ex.Message);
            }
        }

        public static MinMax GetMinMax(Point[] points)
        {

            MinMax minMax = new MinMax();

            if (points == null || points.Length <= 0) return minMax;

            int yMin = 1000000; int yMax = 0;
            int xMin = 1000000; int xMax = 0;

            foreach (Point p in points)
            {
                if (p.X < xMin) xMin = p.X;
                if (p.X > xMax) xMax = p.X;
                if (p.Y < yMin) yMin = p.Y;
                if (p.Y > yMax) yMax = p.Y;
            }

            minMax.MinX = xMin;
            minMax.MaxX = xMax;
            minMax.MinY = yMin;
            minMax.MaxY = yMax;

            return minMax;
        }

        public struct MinMax
        {
            public int MinX;
            public int MaxX;
            public int MinY;
            public int MaxY;
        }

        public struct ScaleFactor
        {
            public float ScaleFactorX;
            public float ScaleFactorY;
        }

        public struct TextAnnotation
        {
            public float X;
            public float Y;
            public string Text;
        }

        public struct LineAnnotation
        {
            public float X1;
            public float Y1;
            public float X2;
            public float Y2;
        }

        public class Annotation
        {
            public LineAnnotation Line { get; set; }
            public TextAnnotation Text { get; set; }
            public Annotation() { }
        }
    }
}
