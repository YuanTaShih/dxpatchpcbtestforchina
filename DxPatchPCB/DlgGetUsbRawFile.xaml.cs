﻿using System.Windows;
using System.Windows.Controls.Primitives;

namespace DxPatchPCBTestApp
{
    public partial class DlgGetUsbRawFile : Window
    {
        public DlgGetUsbRawFile(string rawFilePath) {
            InitializeComponent();
            if(rawFilePath.Substring(rawFilePath.Length - 3, 3).Equals("001"))
                TxtFoundRawFile.Text = "Found Noise file, do you want to process this file ?";
            TxtFoundRawFile.Text += "\n" + rawFilePath;
        }

        private void txtPPID_GetFocus(object sender, RoutedEventArgs e) {
            if (TxtPPID.Text == AppConfig.PPIDPlaceholder) TxtPPID.Text = string.Empty;
        }

        private void txtPPID_LostFocus(object sender, RoutedEventArgs e) {
            if (TxtPPID.Text == string.Empty) TxtPPID.Text = AppConfig.PPIDPlaceholder;
        }

        private void btnNoRunRawFileTest_Click(object sender, RoutedEventArgs e) {
            DialogResult = false; this.Close();
        }

        private void btnYesRunRawFileTest_Click(object sender, RoutedEventArgs e) {
            DialogResult = true; this.Close();
        }

        private void txtPPID_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Enter)
                btnYesRunRawFileTest.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }
    }
}
