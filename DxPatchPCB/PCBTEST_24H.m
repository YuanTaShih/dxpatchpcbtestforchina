function PCBTEST_24H
    clc;clear all;close all;
    global h0

    [FileName,PathName] = uigetfile('*.csv','Select a DxPatch Test csv file','c:\temp\');

    try
        parts = textscan(FileName, '%s', 'delimiter', sprintf('_'));
        Pathfile = [PathName FileName];
        CH =parts{1,1}{2};
        ID =parts{1,1}{4};
        Date = parts{1,1}{5};
%             Time = parts{1,1}{6};
        cd(PathName);

%             temp=dir('*.pdf');
%             if ~isempty(temp),  delete(temp.name); end          

        [total]=TestRun(Pathfile,CH,ID,Date);

        if total == -1
             msgbox('時間長度不足24H','分析失敗');
             print(h0, '-dpdf',['CH_' CH '_ID_' ID '_' Date '_24H_Fail.pdf']);
            return;
        end
        
        temp = exist(Pathfile);
        if temp == 2
            ReFilename = ['CH_'  CH  '_ID_' ID '_' Date '.CSV'];
            movefile(Pathfile, ReFilename);
        end   

        Pathfile = Pathfile(1:length(Pathfile)-4);
        temp = exist(Pathfile);    
        if temp == 2
            ReFilename = ['CH_'  CH  '_ID_' ID '_' Date '.RAW'];
            movefile(Pathfile, ReFilename);
        end          

    catch
        msgbox('分析檔案有誤!','分析失敗');
        try
            print(h0, '-dpdf','Signal.pdf');
        catch
            msgbox('無法匯出報告','分析失敗');
        end
    end
end

function [total]=TestRun(Pathfile,CH,ID,Date)
    global h0
    x = csvread(Pathfile,2);
    show_indcator = false;
    print_all_page = true;
    
    sampling = 500;
    ecg = x(:,2)/200;
    time = (0:length(ecg)-1)/sampling;   

    % 31 spike
    Item = zeros(31,1);
%     temp_step = zeros(31,1);
%     temp_y = ones(31,1)*21;
    
    h0=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 2, 29, 18],'PaperOrientation','landscape');
    subplot(3,1,1);
    text(0, 1 ,sprintf('DxPatch PCB Gain Stability and Time Accuracy Test: CH %s ID %s FAIL',CH,ID),'FontSize',16);
    text(0, 0.75 ,sprintf('Report date: %s ',Date),'FontSize',16);
    text(0, 0.5 ,sprintf('Firmware Version: %s ','K&Y001'),'FontSize',16);
    axis off;
    subplot(3,1,2:3);
    plot(time,ecg);hold on;
    ylabel('mV');xlabel('Time (s)');
    title('Signal graph:','FontWeight','bold','FontSize',16);grid on; grid(gca,'minor');    
    
    if time(length(time))<86400
        total=-1;
        return;
    end    
    
    % first minute
    spike_t = find(ecg(1:180*sampling)>=8);      if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1);  spike_1min=spike_t;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_A=mean(pks_Ref)-mean(npks_Ref);
    clear data temp_time
    
    % second minute
%     plot(time(spike_t+30*sampling:spike_t+90*sampling),ecg(spike_t+30*sampling:spike_t+90*sampling));
    spike_s=spike_t+30*sampling;
    spike_t = find(ecg(spike_t+30*sampling:spike_t+90*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_2min=mean(pks_Ref)-mean(npks_Ref);
    
    % fifth minute
%     plot(time(spike_t+150*sampling:spike_t+210*sampling),ecg(spike_t+150*sampling:spike_t+210*sampling));
    spike_s=spike_t+150*sampling;
    spike_t = find(ecg(spike_t+150*sampling:spike_t+210*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_5min=mean(pks_Ref)-mean(npks_Ref);

    % tenth minute
%     plot(time(spike_t+270*sampling:spike_t+330*sampling),ecg(spike_t+270*sampling:spike_t+330*sampling));
    spike_s=spike_t+270*sampling;
    spike_t = find(ecg(spike_t+270*sampling:spike_t+330*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_10min=mean(pks_Ref)-mean(npks_Ref);    
    
    % twentyth minute
%     plot(time(spike_t+570*sampling:spike_t+630*sampling),ecg(spike_t+570*sampling:spike_t+630*sampling));
    spike_s=spike_t+570*sampling;
    spike_t = find(ecg(spike_t+570*sampling:spike_t+630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_20min=mean(pks_Ref)-mean(npks_Ref);
    
    % thirtyth minute
%     plot(time(spike_t+570*sampling:spike_t+630*sampling),ecg(spike_t+570*sampling:spike_t+630*sampling));
    spike_s=spike_t+570*sampling;
    spike_t = find(ecg(spike_t+570*sampling:spike_t+630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_30min=mean(pks_Ref)-mean(npks_Ref);    
    
    % forty-fifth minute 
%     plot(time(spike_t+870*sampling:spike_t+930*sampling),ecg(spike_t+870*sampling:spike_t+930*sampling));
    spike_s=spike_t+870*sampling;
    spike_t = find(ecg(spike_t+870*sampling:spike_t+930*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_45min=mean(pks_Ref)-mean(npks_Ref);    
    
    % first hour 
%     plot(time(spike_t+870*sampling:spike_t+930*sampling),ecg(spike_t+870*sampling:spike_t+930*sampling));
    spike_s=spike_t+870*sampling;
    spike_t = find(ecg(spike_t+870*sampling:spike_t+930*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_1hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % second hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_2hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % third hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_3hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % 4-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_4hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % 5-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_5hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % 6-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_6hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % 7-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_7hr=mean(pks_Ref)-mean(npks_Ref);    
    
    % 8-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_8hr=mean(pks_Ref)-mean(npks_Ref);        
    
    % 9-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_9hr=mean(pks_Ref)-mean(npks_Ref);        
    
    % 10-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_10hr=mean(pks_Ref)-mean(npks_Ref);        
    
    % 11-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_11hr=mean(pks_Ref)-mean(npks_Ref);   
    
    % 12-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_12hr=mean(pks_Ref)-mean(npks_Ref);   
    
    % 13-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_13hr=mean(pks_Ref)-mean(npks_Ref);   
    
    % 14-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_14hr=mean(pks_Ref)-mean(npks_Ref);
    
    % 15-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_15hr=mean(pks_Ref)-mean(npks_Ref);
    
    % 16-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_16hr=mean(pks_Ref)-mean(npks_Ref);   
    
    % 17-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_17hr=mean(pks_Ref)-mean(npks_Ref);   

    % 18-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_18hr=mean(pks_Ref)-mean(npks_Ref);     
    
    % 19-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_19hr=mean(pks_Ref)-mean(npks_Ref);
    
    % 20-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_20hr=mean(pks_Ref)-mean(npks_Ref);       
    
    % 21-th hour 
    plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_21hr=mean(pks_Ref)-mean(npks_Ref);     
    
    % 22-th hour 
    plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_22hr=mean(pks_Ref)-mean(npks_Ref);     
        
    % 23-th hour 
    plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_23hr=mean(pks_Ref)-mean(npks_Ref);     
        
    % 24-th hour 
%     plot(time(spike_t+3570*sampling:spike_t+3630*sampling),ecg(spike_t+3570*sampling:spike_t+3630*sampling));
    spike_s=spike_t+3570*sampling;
    spike_t = find(ecg(spike_t+3570*sampling:spike_t+3630*sampling)>=8);   if isempty(spike_t), total=-1; return; end
    spike_t = spike_t(1) + spike_s;
    data = ecg(spike_t-11*sampling:spike_t-1*sampling); % 前11秒到前一秒
    temp_time = time(spike_t-11*sampling:spike_t-1*sampling); 
    [pks_Ref,locs,npks_Ref,nlocs]=findpeakdata(data,temp_time,sampling,5);
    ref_24hr=mean(pks_Ref)-mean(npks_Ref);     
        
   
    
    
%     plot(ecg(1:90*sampling));hold on;
%     plot(spike(1),ecg(spike(1)),'ro');
%     plot(time(spike_t+30*sampling:spike_t+90*sampling),ecg(spike_t+30*sampling:spike_t+90*sampling));
    



%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     %% DynamicRange
%     h2=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     %DynamicRange1
%     h2_1=subplot(311); 
%     plot(Dynamic_t1, Dynamic_1);hold on;     text(Dynamic_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn1)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('DynamicRange, Tri, amp=6mV, f=10.4Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([Dynamic_t1(1) Dynamic_t1(length(Dynamic_t1)) -11 11]);grid on; grid(gca,'minor');
%     %DynamicRange2
%     h2_2=subplot(312); 
%     plot(Dynamic_t2,Dynamic_2);hold on;      text(Dynamic_t2(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn2)); 
%     ylabel('mV');xlabel('Time (s)');       
%     title('DynamicRange, Tri, amp=6mV, f=10.4Hz, InputImped.=true, dcoffset=300, duration=40s, width=0');
%     axis([Dynamic_t2(1) Dynamic_t2(length(Dynamic_t2)) -11 11]);grid on; grid(gca,'minor');
%     %DynamicRange3
%     h2_3=subplot(313); 
%     plot(Dynamic_t3,Dynamic_3);hold on;       text(Dynamic_t3(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn3)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('DynamicRange, Tri, amp=6mV, f=10.4Hz, InputImped.=true, dcoffset=-300, duration=40s, width=0');
%     axis([Dynamic_t3(1) Dynamic_t3(length(Dynamic_t3)) -11 11]);grid on; grid(gca,'minor');
% 
% 
%     h3=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     % DynamicRange1
%     h3_1=subplot(311); 
%     plot(Dynamic_t4,Dynamic_4);hold on;         text(Dynamic_t4(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn4)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('DynamicRange, Tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([Dynamic_t4(1) Dynamic_t4(length(Dynamic_t4)) -11 11]);grid on; grid(gca,'minor');
%     % DynamicRange2
%     h3_2=subplot(312); 
%     plot(Dynamic_t5,Dynamic_5);hold on;           text(Dynamic_t5(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn5)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('DynamicRange, Tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=300, duration=40s, width=0');
%     axis([Dynamic_t5(1) Dynamic_t5(length(Dynamic_t5)) -11 11]);grid on; grid(gca,'minor');
%     % DynamicRange3
%     h3_3=subplot(313); 
%     plot(Dynamic_t6,Dynamic_6);hold on;           text(Dynamic_t6(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn6)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('DynamicRange, Tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=-300, duration=40s, width=0');
%     axis([Dynamic_t6(1) Dynamic_t6(length(Dynamic_t6)) -11 11]);grid on; grid(gca,'minor');
% 
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     %% InputImpedance
%     h4=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     % InputImpedance1
%     h4_1=subplot(411); 
%     plot(InputImp_t1,InputImp_1);hold on;      text(InputImp_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp1)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([InputImp_t1(1) InputImp_t1(length(InputImp_t1)) -11 11]);grid on; grid(gca,'minor');
%     % InputImpedance2
%     h4_2=subplot(412); 
%     plot(InputImp_t2,InputImp_2);hold on;          text(InputImp_t2(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp2)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=0, duration=40s, width=0');
%     axis([InputImp_t2(1) InputImp_t2(length(InputImp_t2)) -11 11]);grid on; grid(gca,'minor');
%     % InputImpedance3
%     h4_3=subplot(413); 
%     plot(InputImp_t3,InputImp_3);hold on;          text(InputImp_t3(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp3)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=300, duration=40s, width=0');
%     axis([InputImp_t3(1) InputImp_t3(length(InputImp_t3)) -11 11]);grid on; grid(gca,'minor');
%     % InputImpedance4
%     h4_4=subplot(414); 
%     plot(InputImp_t4,InputImp_4);hold on;           text(InputImp_t4(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp4)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=-300, duration=40s, width=0');
%     axis([InputImp_t4(1) InputImp_t4(length(InputImp_t4)) -11 11]);grid on; grid(gca,'minor');
% 
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     h5=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     % GainAccuracy1
%     h5_1=subplot(311); 
%     plot(GainAccuracy_t1,GainAccuracy_1);hold on;     text(GainAccuracy_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_GainAccuracy1)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('GainAccuracy, Sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([GainAccuracy_t1(1) GainAccuracy_t1(length(GainAccuracy_t1)) -11 11]);grid on; grid(gca,'minor');
%     % GainStability1
%     h5_2=subplot(312); 
%     plot(GainStability_t1,GainStability_1);hold on;    text(GainStability_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_GainStability1)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('GainStability, Sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=150s, width=0');
%     axis([GainStability_t1(1) GainStability_t1(length(GainStability_t1)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-RectanglePulse1
%     h5_3=subplot(313); 
%     plot(FreqResRect_t1,FreqResRect_1);hold on;      text(FreqResRect_t1(1), 9 ,sprintf('Before: %0.4fmV, After: %0.4fmV, Slope: %0.4fmV/s', v_before,v_after,slope)); 
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, RectanglePulse, amp=3mV, f=0.05Hz, InputImped.=true, dcoffset=0, duration=80s, width=100'); 
%     axis([FreqResRect_t1(1) FreqResRect_t1(length(FreqResRect_t1)) -11 11]);grid on; grid(gca,'minor');
% 
% 
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     h6=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     % FrequencyResponse-Sin1
%     h6_1=subplot(411); 
%     plot(FreqResSin_t1,FreqResSin_1);hold on;         text(FreqResSin_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin1));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=0.1Hz, InputImped.=true, dcoffset=0, duration=80s, width=0');
%     axis([FreqResSin_t1(1) FreqResSin_t1(length(FreqResSin_t1)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Sin2
%     h6_2=subplot(412); 
%     plot(FreqResSin_t2,FreqResSin_2);hold on;            text(FreqResSin_t2(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin2));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=0.67Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t2(1) FreqResSin_t2(length(FreqResSin_t2)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Sin3
%     h6_3=subplot(413); 
%     plot(FreqResSin_t3,FreqResSin_3);hold on;              text(FreqResSin_t3(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin3));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=1Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t3(1) FreqResSin_t3(length(FreqResSin_t3)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Sin4
%     h6_4=subplot(414); 
%     plot(FreqResSin_t4,FreqResSin_4);hold on;             text(FreqResSin_t4(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin4));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=2Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t4(1) FreqResSin_t4(length(FreqResSin_t4)) -11 11]);grid on; grid(gca,'minor');
% 
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     h7=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     % FrequencyResponse-Sin5
%     h7_1=subplot(411); 
%     plot(FreqResSin_t5,FreqResSin_5);hold on;               text(FreqResSin_t5(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin5));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t5(1) FreqResSin_t5(length(FreqResSin_t5)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Sin6
%     h7_2=subplot(412); 
%     plot(FreqResSin_t6,FreqResSin_6);hold on;               text(FreqResSin_t6(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin6));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t6(1) FreqResSin_t6(length(FreqResSin_t6)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Sin7
%     h7_3=subplot(413); 
%     plot(FreqResSin_t7,FreqResSin_7);hold on;               text(FreqResSin_t7(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin7));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=20Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t7(1) FreqResSin_t7(length(FreqResSin_t7)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Sin8
%     h7_4=subplot(414); 
%     plot(FreqResSin_t8,FreqResSin_8);hold on;                 text(FreqResSin_t8(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin8));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=40Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t8(1) FreqResSin_t8(length(FreqResSin_t8)) -11 11]);grid on; grid(gca,'minor');
% 
% 
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     h8=figure('Visible', 'off');
%     set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
%     % FrequencyResponse-Sin9
%     h8_1=subplot(411); 
%     plot(FreqResSin_t9,FreqResSin_9);hold on;                   text(FreqResSin_t9(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin9));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Sin, amp=2mV, f=55Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
%     axis([FreqResSin_t9(1) FreqResSin_t9(length(FreqResSin_t9)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Tri1
%     h8_2=subplot(412); 
%     plot(FreqResTri_t1,FreqResTri_1);hold on;          text(FreqResTri_t1(1), 9 ,sprintf('pk: %.4f mV',ref_FreqResTri1));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse, Tri-pulse, amp=1.5mV, f=1Hz, InputImped.=true, dcoffset=0, duration=15s, width=200');          
%     axis([FreqResTri_t1(1) FreqResTri_t1(length(FreqResTri_t1)) -11 11]);grid on; grid(gca,'minor');
%     % FrequencyResponse-Tri2
%     h8_3=subplot(413); 
%     plot(FreqResTri_t2,FreqResTri_2);hold on;           text(FreqResTri_t2(1), 9 ,sprintf('pk: %.4f mV',ref_FreqResTri2));
%     ylabel('mV');xlabel('Time (s)');
%     title('FrequencyResponse,  Tri-pulse, amp=1.5mV, f=1Hz, InputImped.=true, dcoffset=0, duration=150s, width=40');          
%     axis([FreqResTri_t2(1) FreqResTri_t2(length(FreqResTri_t2)) -11 11]);grid on; grid(gca,'minor');


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% first page
    h1=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    
    subplot(3,1,1:2);
    x=1;d=0.03;
    text(-0.1, x ,sprintf('1. Gain Stability: Sine, f=5Hz, Amp=2mV, DC offser=0mV, Standard <= |A +/- A*0.03|'),'FontWeight','bold'); x = x - d;
    text(-0.06, x ,sprintf('(1). 1 min:'));text(0.45,x,sprintf('ref: %0.4f mV',ref_A));
    x = x - d;
    
    k=1;%number
    Qua=abs((ref_2min-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 2 min:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_2min));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;
    
    Qua=abs((ref_5min-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 5 min:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_5min));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;
    
    Qua=abs((ref_10min-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 10 min:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_10min));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1; 
    
    Qua=abs((ref_20min-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 20 min:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_20min));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;  
    
    Qua=abs((ref_30min-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 30 min:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_30min));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;        
    
    Qua=abs((ref_45min-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 45 min:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_45min));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;        
    
    Qua=abs((ref_1hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 1 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_1hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            
    
    Qua=abs((ref_2hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 2 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_2hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            
     
    Qua=abs((ref_3hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 3 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_3hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;         
    
    Qua=abs((ref_4hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 4 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_4hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;          
    
    Qua=abs((ref_5hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 5 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_5hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;     

    Qua=abs((ref_6hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 6 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_6hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            

    Qua=abs((ref_7hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 7 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_7hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;       
    
    Qua=abs((ref_8hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 8 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_8hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;   
    
    Qua=abs((ref_9hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 9 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_9hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;      
    
    Qua=abs((ref_10hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 10 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_10hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            
    
    Qua=abs((ref_11hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 11 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_11hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            
    
    Qua=abs((ref_12hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 12 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_12hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            
     
    Qua=abs((ref_13hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 13 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_13hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;         
    
    Qua=abs((ref_14hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 14 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_14hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;          
    
    Qua=abs((ref_15hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 15 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_15hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;     

    Qua=abs((ref_16hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 16 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_16hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;            

    Qua=abs((ref_17hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 17 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_17hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;       
    
    Qua=abs((ref_18hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 18 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_18hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;   
    
    Qua=abs((ref_19hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 19 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_19hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;      
    
    Qua=abs((ref_20hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 20 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_20hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;   
    
    Qua=abs((ref_21hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 21 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_21hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;      
    
    Qua=abs((ref_22hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 22 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_22hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;      
    
    Qua=abs((ref_23hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 23 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_23hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;       
    
    Qua=abs((ref_24hr-ref_A)/ref_A)*100; if Qua<=3, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(%d). 24 hour:',k+1));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_24hr));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;k=k+1;         
        
    
    text(-0.1, x ,sprintf('2. Time Accuracy: Skike wave, Standard: 24H +/- 30s'),'FontWeight','bold'); x = x - d;
    
    Qua= abs((time(spike_t)-time(spike_1min)) - (24*60*60-60)); if Qua<=30, str = 'Pass'; Item(k)=1; str_c ='blue'; else str = 'False';Item(k)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('(1). 24 hour:'));text(0.45,x,sprintf('Time = %0.1fs,',time(spike_t)-time(spike_1min)+60));
    text(0.65,x,sprintf('Diff (s)= %0.1f s,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d; 
    

    text(-0.1, 1.13 ,sprintf('DxPatch PCB Performance Test: CH %s ID %s',CH,ID),'FontSize',16);
    text(-0.1, 1.10 ,sprintf('Report date: %s ',Date),'FontSize',8);
    text(-0.1, 1.08 ,sprintf('Firmware Version: %s ','K&Y001'),'FontSize',8);
    axis off;
    
    subplot(313);
    plot(time,ecg);hold on;
    ylabel('mV');xlabel('Time (s)');
    title('Signal graph:','FontWeight','bold','FontSize',16);grid on; grid(gca,'minor');
    axis([time(1) time(spike_t) -11 11]);
  

    
%     if show_indcator == true
%          plot(h2_1,locs_Dyn1, pks_Dyn1,'ro',nlocs_Dyn1,npks_Dyn1,'mo');
%          plot(h2_2,locs_Dyn2, pks_Dyn2,'ro',nlocs_Dyn2,npks_Dyn2,'mo'); 
%          plot(h2_3,locs_Dyn3, pks_Dyn3,'ro',nlocs_Dyn3,npks_Dyn3,'mo'); 
%          plot(h3_1,locs_Dyn4, pks_Dyn4,'ro',nlocs_Dyn4,npks_Dyn4,'mo');
%          plot(h3_2,locs_Dyn5, pks_Dyn5,'ro',nlocs_Dyn5,npks_Dyn5,'mo');
%          plot(h3_3,locs_Dyn6, pks_Dyn6,'ro',nlocs_Dyn6,npks_Dyn6,'mo');
%          plot(h4_1,locs_InputImp1, pks_InputImp1,'ro',nlocs_InputImp1,npks_InputImp1,'mo'); 
%          plot(h4_2,locs_InputImp2, pks_InputImp2,'ro',nlocs_InputImp2,npks_InputImp2,'mo');
%          plot(h4_3,locs_InputImp3, pks_InputImp3,'ro',nlocs_InputImp3,npks_InputImp3,'mo'); 
%          plot(h4_4,locs_InputImp4, pks_InputImp4,'ro',nlocs_InputImp4,npks_InputImp4,'mo');
%          plot(h5_1,locs_GainAccuracy1, pks_GainAccuracy1,'ro',nlocs_GainAccuracy1,npks_GainAccuracy1,'mo');  
%          plot(h5_2,locs_GainStability1, pks_GainStability1,'ro',nlocs_GainStability1,npks_GainStability1,'mo');
%          plot(h5_3,slope_line,slope_v,'g');plot(h5_3,i_before, v_before,'r', i_after, v_after,'m');
%          plot(h6_1,locs_FreqResSin1, pks_FreqResSin1,'ro',nlocs_FreqResSin1,npks_FreqResSin1,'mo'); 
%          plot(h6_2,locs_FreqResSin2, pks_FreqResSin2,'ro',nlocs_FreqResSin2,npks_FreqResSin2,'mo'); 
%          plot(h6_3,locs_FreqResSin3, pks_FreqResSin3,'ro',nlocs_FreqResSin3,npks_FreqResSin3,'mo');
%          plot(h6_4,locs_FreqResSin4, pks_FreqResSin4,'ro',nlocs_FreqResSin4,npks_FreqResSin4,'mo');  
%          plot(h7_1,locs_FreqResSin5, pks_FreqResSin5,'ro',nlocs_FreqResSin5,npks_FreqResSin5,'mo'); 
%          plot(h7_2,locs_FreqResSin6, pks_FreqResSin6,'ro',nlocs_FreqResSin6,npks_FreqResSin6,'mo'); 
%          plot(h7_3,locs_FreqResSin7, pks_FreqResSin7,'ro',nlocs_FreqResSin7,npks_FreqResSin7,'mo'); 
%          plot(h7_4,locs_FreqResSin8, pks_FreqResSin8,'ro',nlocs_FreqResSin8,npks_FreqResSin8,'mo'); 
%          plot(h8_1,locs_FreqResSin9, pks_FreqResSin9,'ro',nlocs_FreqResSin9,npks_FreqResSin9,'mo');
%          plot(h8_2,locs_FreqResTri1, pks_FreqResTri1,'ro',i_base1,v_base1,'m'); 
%          plot(h8_3,locs_FreqResTri2, pks_FreqResTri2,'ro',i_base2,v_base2,'m'); 
%     end
%     
    total = sum(Item);
    
    if total==31, str1 = 'Pass'; color = 'g';  else str1 = 'FAIL'; color = 'r'; end
    str = sprintf( '結果: %s',str1);
    
    ResultFilename = ['CH_'  CH  '_ID_' ID '_' Date '_24H_' str1 '.pdf'];
    print(h1, '-dpdf',ResultFilename);
%     if print_all_page == true
%         print(h2, '-dpdf','Page2.pdf');
%         print(h3, '-dpdf','Page3.pdf');
%         print(h4, '-dpdf','Page4.pdf');
%         print(h5, '-dpdf','Page5.pdf');
%         print(h6, '-dpdf','Page6.pdf');
%         print(h7, '-dpdf','Page7.pdf');
%         print(h8, '-dpdf','Page8.pdf');
%     end

    hm=msgbox(str,'分析完成');
    set(hm, 'units', 'normalized', 'position', [0.4 0.5 0.4 0.2]); %makes box bigger
    ah = get( hm, 'CurrentAxes' );
    ch = get( ah, 'Children' );
    set( ch, 'FontSize', 80 ,'Color',color); %makes text bigger    
end

function [pks,locs,npks,nlocs]=findpeakdata(data,time,sampling,Hz)
    duration = ceil((sampling/Hz)/2);
    [pks,locs] = findpeaks(data,'MinPeakDistance',duration,'MINPEAKHEIGHT',0.1); 
    [npks,nlocs] = findpeaks(-data,'MinPeakDistance',duration,'MINPEAKHEIGHT',0.1); 
    
    if locs(1)<nlocs(1)
        locs(1)=[];pks(1)=[];
    else
        nlocs(1)=[];npks(1)=[];
    end
    
    if locs(length(locs))>nlocs(length(nlocs))
        locs(length(locs))=[]; pks(length(locs))=[];
    else
        nlocs(length(nlocs))=[];npks(length(nlocs))=[];
    end
    
    locs = time(locs); 
    nlocs = time(nlocs);
    npks = -npks;
end


function  [pks,locs,baseline_v,baseline_i] = findbaseline2(data,time,sampling)
    [pks,locs] = findpeaks(data,'MinPeakDistance',sampling/10,'MINPEAKHEIGHT',0.3);
    pks=pks(2);locs=locs(2);
    baseline_i = time(locs+sampling/4:locs+sampling/2);
    baseline_v = data(locs+sampling/4:locs+sampling/2);
    locs = time(locs);
end
