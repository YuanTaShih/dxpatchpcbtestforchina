﻿using System;
using System.Collections.Generic;
using System.Xml;
using WhaleTeqSECG_SDK;

namespace DxPatchPCBTestApp
{
    public class SignalTask
    {
        public List<OutputLead_E> outputLeads;
        public OutputFunction outputFunc;
        public SignalTestFunc testFunc;
        public double duration;
        public double frequency;
        public double amplitude;
        public int dcOffset;
        public bool inputImpedance;
        public DateTime dateTimeExecuted;
        //YT Shih
        public int width;

        public SignalTask() {}

        public SignalTask(XmlAttributeCollection xmlAttr)
        {
            if (xmlAttr["outputLeads"] != null) {

                outputLeads = new List<OutputLead_E>();

                List<string> leads = new List<string>();
                leads.Add( xmlAttr["outputLeads"].Value );                

                foreach (string strLead in leads)
                {
                    this.outputLeads.Add(
                        (OutputLead_E)Enum.Parse(typeof(OutputLead_E), strLead, true)
                    );
                }
            }

            if(xmlAttr["outputFunc"] != null) { 
                this.outputFunc = (OutputFunction)Enum.Parse(
                    typeof(OutputFunction), xmlAttr["outputFunc"].Value, true);
            }

            if (xmlAttr["testFunc"] != null) {
                foreach (SignalTestFunc f in Enum.GetValues(typeof(SignalTestFunc))) {
                    if (f.ToString().ToUpper() == xmlAttr["testFunc"].Value.ToUpper()) {
                        this.testFunc = f;
                        break;
                    }
                }
            }

            if (xmlAttr["amplitude"] != null)
                this.amplitude = Double.Parse(xmlAttr["amplitude"].Value);

            if (xmlAttr["frequency"] != null)
                this.frequency = Double.Parse(xmlAttr["frequency"].Value);

            if (xmlAttr["dcOffset"] != null)
                this.dcOffset = Int32.Parse(xmlAttr["dcOffset"].Value);

            if (xmlAttr["inputImpedance"] != null)
                this.inputImpedance = Boolean.Parse(xmlAttr["inputImpedance"].Value);

            if (xmlAttr["duration"] != null)
                this.duration = Int32.Parse(xmlAttr["duration"].Value);

            if (xmlAttr["width"] != null)
                this.width = Int32.Parse(xmlAttr["width"].Value);
        }

        public object toDataRow()
        {
            string strLeads = "";
            foreach (OutputLead_E lead in outputLeads) {
                strLeads += lead.ToString().Replace("Lead_", "");
            }

            var row = new {
                outputLead = strLeads,
                outputFunc = outputFunc.ToString().Replace("Output_", ""),
                duration = duration,
                frequency = frequency,
                amplitude = amplitude,
                dcOffset = dcOffset,
                width = width
            };

            return row;
        }

        public string toItemString()
        {
            string info = "";
            //if(simNoise >= 0) info += " simNoise " + simNoise;
            if(outputLeads.Count >= 1) {
                info += outputLeads[0].ToString().Replace("Lead_", "") + "\t";
            }
            
            if (outputFunc != OutputFunction.Output_Off) {
                info += outputFunc.ToString() + "\t";
            } else {
                info += "\t\t";
            } 

            info += " time " + duration.ToString("0.0") + "\t";
            info += " freq " + frequency.ToString("0.0") + "\t";
            info += " ampl " + amplitude.ToString("0.0") + "\t";
            info += " dcOffset " + dcOffset + "\t";
            return info;
        }
    }
}
