﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DxPatchPCBTestApp
{
    static class StringTools
    {
        public static string SpaceCamelCase(string input)
        {
            List<int> insertIndexes = new List<int>();
            char[] chars = input.ToArray<char>();

            for(int i = 1; i < chars.Length; i++){
                if (char.IsUpper(chars[i])){
                    insertIndexes.Add(i);
                }
            }

            foreach (int i in insertIndexes){
                input = input.Insert(i, " ");
            }

            return input;
        }
    }
}
