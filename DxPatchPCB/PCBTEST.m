function PCBTEST
    clc;clear all;close all;
    global h0
%     x = csvread('C:\temp\CH_02_ID_11308\CH02ID11308_20170206_061043_FAIL.RAW.csv',2);
%     x = csvread('C:\temp\CH_02_ID_11308\CH_02_ID_11308_20170209_024917_FAIL.RAW.csv',2);
%     x = csvread('C:\temp\CH_02_ID_11308\CH_02_ID_11308_20170209_053847_FAIL.RAW.csv',2);

    [FileName,PathName] = uigetfile('*.csv','Select a DxPatch Test csv file','c:\temp\');

    
    
    if strfind(FileName,'NOISE')
        NOISE = true;
    else
        NOISE = false;
    end
    
    if NOISE
        try
            parts = textscan(FileName, '%s', 'delimiter', sprintf('_'));
            Pathfile = [PathName FileName];
            CH =parts{1,1}{2};
            ID =parts{1,1}{4};
            Date = parts{1,1}{5};
%             Time = parts{1,1}{6};
            cd(PathName);
            
%             temp=dir('*.pdf');
%             if ~isempty(temp),  delete(temp.name); end          
            
            TestNoiseRun(Pathfile,CH,ID,Date);
            
            temp = exist(Pathfile);
            if temp == 2
                ReFilename = ['CH_'  CH  '_ID_' ID '_' Date '._NOISE.CSV'];
                movefile(Pathfile, ReFilename);
            end  
            
            Pathfile = Pathfile(1:length(Pathfile)-4);
            temp = exist(Pathfile);    
            if temp == 2
                ReFilename = ['CH_'  CH  '_ID_' ID '_' Date '_NOISE.RAW'];
                movefile(Pathfile, ReFilename);
            end          
        catch
            msgbox('分析檔案有誤!','分析失敗');
            try
                print(h0, '-dpdf','NOISE_Signal.pdf');
            catch
                msgbox('無法匯出報告','分析失敗');
            end
        end
    else
        try
            parts = textscan(FileName, '%s', 'delimiter', sprintf('_'));
            Pathfile = [PathName FileName];
            CH =parts{1,1}{2};
            ID =parts{1,1}{4};
            Date = parts{1,1}{5};
%             Time = parts{1,1}{6};
            cd(PathName);        
%             temp=dir('*.pdf');
%             if ~isempty(temp),  delete(temp.name); end          
            
            TestRun(Pathfile,CH,ID,Date);
            
            
            temp = exist(Pathfile);
            if temp == 2
                ReFilename = ['CH_'  CH  '_ID_' ID '_' Date '.CSV'];
                movefile(Pathfile, ReFilename);
            end   
            
            Pathfile = Pathfile(1:length(Pathfile)-4);
            temp = exist(Pathfile);    
            if temp == 2
                ReFilename = ['CH_'  CH  '_ID_' ID '_' Date '.RAW'];
                movefile(Pathfile, ReFilename);
            end          
            
        catch
            msgbox('分析檔案有誤!','分析失敗');
            try
                print(h0, '-dpdf','Signal.pdf');
            catch
                msgbox('無法匯出報告','分析失敗');
            end
        end
    end
end

function [total]=TestNoiseRun(Pathfile,CH,ID,Date)
    global h0
    x = csvread(Pathfile,2);
    show_indcator = false;
    sampling = 500;
    
    ecg = x(:,2)/200;
    time = (0:length(ecg)-1)/sampling;
    
    h0=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 2, 29, 18],'PaperOrientation','landscape');
    subplot(3,1,1);
    text(0, 1 ,sprintf('DxPatch PCB 51K and 47nF Noise Test: CH %s ID %s FAIL',CH,ID),'FontSize',16);
    text(0, 0.75 ,sprintf('Report date: %s ',Date),'FontSize',16);
    text(0, 0.5 ,sprintf('Firmware Version: %s ','K&Y001'),'FontSize',16);
    axis off;
    
    subplot(3,1,2:3);
    plot(time,ecg);hold on;
    ylabel('mV');xlabel('Time (s)');
    %stem(time(locs),pks,'r');
    title('Signal graph:','FontWeight','bold','FontSize',16);grid on; grid(gca,'minor');
%     plot(time,ecg_marker,'r');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Item=zeros(3,1);
  
    %%%%%%% Step1: off  %%%%%%%%%%%
    signal_1 = ecg(20*sampling:30*sampling);
    signal_t1 = time(20*sampling:30*sampling);
    
    [pks_Noise1,locs_Noise1,npks_Noise1,nlocs_Noise1] = findpeakdata(signal_1,signal_t1,sampling,50,0.01);
    ref_Noise1=mean(pks_Noise1)-mean(npks_Noise1);

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Noise
    h1=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    %CMRR
    h1_2=subplot(512); 
    plot(time, ecg);hold on;    
    ylabel('mV');xlabel('Time (s)');
    title('Noise, Input=0mV(p-v), f=0Hz, Power Line');
    axis([time(1) time(length(time)) -11 11]);grid on; grid(gca,'minor');   
    
    
    %CMRR
    h1_3=subplot(513); 
    plot(signal_t1, signal_1);hold on;     text(signal_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_Noise1)); 
    ylabel('mV');xlabel('Time (s)');
    title('Noise, Input=0mV, f=0Hz, dcoffset=off, duration=40s');
    axis([signal_t1(1) signal_t1(length(signal_t1)) -0.5 0.5]);grid on; grid(gca,'minor');


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   h1_1=subplot(511);
   x=1;d=0.1;
   text(-0.1, x ,sprintf('1. Noise: f=0Hz, Input=0mV(p-v), Standard <= 0.05mV'),'FontWeight','bold'); x = x - d;%     
   text(-0.06, x ,sprintf('A. DC offset = 0mV:'));text(0.45,x,sprintf('ref: %0.4f mV',4));
   Qua=ref_Noise1; if Qua<=0.05, str = 'Pass'; Item(1)=1; str_c ='blue'; else str = 'False';Item(1)=0; str_c = 'red'; end
   text(0.65,x,sprintf('V(p-v) = %0.4fmV,',Qua));
   text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;

    text(-0.1, 1.5 ,sprintf('DxPatch PCB 51K and 47nF Noise Test: CH %s ID %s',CH,ID),'FontSize',16);
    text(-0.1, 1.35 ,sprintf('Report date: %s ',Date),'FontSize',8);
    text(-0.1, 1.25 ,sprintf('Firmware Version: %s ','K&Y001'),'FontSize',8);
    axis off;

    total = sum(Item);
     
    if show_indcator == true
         plot(h1_3,locs_Noise1, pks_Noise1,'ro',nlocs_Noise1,npks_Noise1,'mo'); 
    end
     
    if total==1, str1 = 'Pass'; color = 'g';  else str1 = 'FAIL'; color = 'r'; end
    str = sprintf( '結果: %s',str1);

    ResultFilename = ['CH_'  CH  '_ID_' ID '_' Date '_NOISE_' str1 '.pdf'];
    print(h1, '-dpdf',ResultFilename);

    hm=msgbox(str,'分析完成');
    set(hm, 'units', 'normalized', 'position', [0.4 0.5 0.4 0.2]); %makes box bigger
    ah = get( hm, 'CurrentAxes' );
    ch = get( ah, 'Children' );
    set( ch, 'FontSize', 80 ,'Color',color); %makes text bigger
end

function [total]=TestRun(Pathfile,CH,ID,Date)
    global h0
    x = csvread(Pathfile,2);
    show_indcator = false;
    print_all_page = true;
    
    % x = csvread('C:\temp\CH_02_ID_11308\TEST.csv',2);
    sampling = 500;
    ecg = x(:,2)/200;

    % [b,a] = butter(5,0.1/(sampling/2),'high');  
    % x1 = filtfilt(b,a,ecg);
    ecg_marker = diff(ecg);
    ecg_marker = abs(ecg_marker);

    temp_step = zeros(19,1);
    temp_y = ones(19,1)*21;
    [~,temp_step(1)] = max(ecg_marker(1:10000));
    
    Item=zeros(14,1);

    time = (0:length(ecg)-1)/sampling;
    % find first 2047
    temp_step(2) = temp_step(1) + 20*sampling;  % DynamicRange, tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(3) = temp_step(2) + 40*sampling;  % DynamicRange, tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=300, duration=40000, width=0
    temp_step(4) = temp_step(3) + 40*sampling;  % DynamicRange, tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=-300, duration=40000, width=0
    temp_step(5) = temp_step(4) + 40*sampling;  % InputImpedance, sin, amp=5mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(6) = temp_step(5) + 40*sampling;  % InputImpedance, sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=0, duration=40000, width=0
    temp_step(7) = temp_step(6) + 40*sampling;  % InputImpedance, sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=300, duration=40000, width=0
    temp_step(8) = temp_step(7) + 40*sampling;  % InputImpedance, sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=-300, duration=40000, width=0
    temp_step(9) = temp_step(8) + 40*sampling;  % GainAccuracy, sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(10) = temp_step(9) + 40*sampling;  % GainStability, sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=150000, width=0
    temp_step(11) = temp_step(10) + 150*sampling;  % MinSignal, sin, amp=0.05mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(12) = temp_step(11) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=0.67Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(13) = temp_step(12) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=1Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(14) = temp_step(13) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=2Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(15) = temp_step(14) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(16) = temp_step(15) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(17) = temp_step(16) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=20Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(18) = temp_step(17) + 40*sampling;  % FrequencyResponse-sin, sin, amp=2mV, f=40Hz, InputImped.=true, dcoffset=0, duration=40000, width=0
    temp_step(19) = temp_step(18) + 40*sampling;  % END
    h0=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 2, 29, 18],'PaperOrientation','landscape');
    subplot(3,1,1);
    text(0, 1 ,sprintf('DxPatch PCB Performance Test: CH %s ID %s FAIL',CH,ID),'FontSize',16);
    text(0, 0.75 ,sprintf('Report date: %s ',Date),'FontSize',16);
    text(0, 0.5 ,sprintf('Firmware Version: %s ','K&Y001'),'FontSize',16);
    axis off;
    subplot(3,1,2:3);
    plot(time,ecg);hold on;
    ylabel('mV');xlabel('Time (s)');
    stem(time(temp_step),temp_y,'r');
    title('Signal graph:','FontWeight','bold','FontSize',16);grid on; grid(gca,'minor');

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% DynamicRange1
    temp_t = temp_step(3)-10*sampling : temp_step(3)-5*sampling;
    Dynamic_1 = ecg(temp_t);
    Dynamic_t1 = time(temp_t);
    [pks_Dyn1,locs_Dyn1,npks_Dyn1,nlocs_Dyn1] = findpeakdata(Dynamic_1,Dynamic_t1,sampling,10.4); 
    ref_Dyn1=mean(pks_Dyn1)-mean(npks_Dyn1);
    %% DynamicRange2
    temp_t = temp_step(4)-10*sampling : temp_step(4)-5*sampling;
    Dynamic_2 = ecg(temp_t);
    Dynamic_t2 = time(temp_t);
    [pks_Dyn2,locs_Dyn2,npks_Dyn2,nlocs_Dyn2] = findpeakdata(Dynamic_2,Dynamic_t2,sampling,10.4); 
    ref_Dyn2=mean(pks_Dyn2)-mean(npks_Dyn2);
    %% DynamicRange3
    temp_t = temp_step(5)-10*sampling : temp_step(5)-5*sampling;
    Dynamic_3 = ecg(temp_t);
    Dynamic_t3 = time(temp_t);    
    [pks_Dyn3,locs_Dyn3,npks_Dyn3,nlocs_Dyn3] = findpeakdata(Dynamic_3,Dynamic_t3,sampling,10.4); 
    ref_Dyn3=mean(pks_Dyn3)-mean(npks_Dyn3);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% InputImpedance1
    temp_t = temp_step(6)-10*sampling : temp_step(6)-5*sampling;
    InputImp_1 = ecg(temp_t);
    InputImp_t1 = time(temp_t);
    [pks_InputImp1,locs_InputImp1,npks_InputImp1,nlocs_InputImp1] = findpeakdata(InputImp_1,InputImp_t1,sampling,10); 
    ref_InputImp1=mean(pks_InputImp1)-mean(npks_InputImp1);
    %% InputImpedance2
    temp_t = temp_step(7)-10*sampling : temp_step(7)-5*sampling;
    InputImp_2 = ecg(temp_t);
    InputImp_t2 = time(temp_t);
    [pks_InputImp2,locs_InputImp2,npks_InputImp2,nlocs_InputImp2] = findpeakdata(InputImp_2,InputImp_t2,sampling,10); 
    ref_InputImp2=mean(pks_InputImp2)-mean(npks_InputImp2);
    %% InputImpedance3
    temp_t = temp_step(8)-10*sampling : temp_step(8)-5*sampling;
    InputImp_3 = ecg(temp_t);
    InputImp_t3 = time(temp_t);
    [pks_InputImp3,locs_InputImp3,npks_InputImp3,nlocs_InputImp3] = findpeakdata(InputImp_3,InputImp_t3,sampling,10); 
    ref_InputImp3=mean(pks_InputImp3)-mean(npks_InputImp3);
    %% InputImpedance4
    temp_t = temp_step(9)-10*sampling : temp_step(9)-5*sampling;
    InputImp_4 = ecg(temp_t);
    InputImp_t4 = time(temp_t);
    [pks_InputImp4,locs_InputImp4,npks_InputImp4,nlocs_InputImp4] = findpeakdata(InputImp_4,InputImp_t4,sampling,10); 
    ref_InputImp4=mean(pks_InputImp4)-mean(npks_InputImp4);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% GainAccuracy1
    temp_t = temp_step(10)-10*sampling : temp_step(10)-5*sampling;
    GainAccuracy_1 = ecg(temp_t);
    GainAccuracy_t1 = time(temp_t);
    [pks_GainAccuracy1,locs_GainAccuracy1,npks_GainAccuracy1,nlocs_GainAccuracy1] = findpeakdata(GainAccuracy_1,GainAccuracy_t1,sampling,5); 
    ref_GainAccuracy1=mean(pks_GainAccuracy1)-mean(npks_GainAccuracy1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% GainStability1
    temp_t = temp_step(11)-10*sampling : temp_step(11)-5*sampling;
    GainStability_1 = ecg(temp_t);
    GainStability_t1 = time(temp_t);
    [pks_GainStability1,locs_GainStability1,npks_GainStability1,nlocs_GainStability1] = findpeakdata(GainStability_1,GainStability_t1,sampling,5); 
    ref_GainStability1=mean(pks_GainStability1)-mean(npks_GainStability1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% MinimalSignal
    temp_t = temp_step(12)-10*sampling : temp_step(12)-5*sampling;
    MinimalSignal_1 = ecg(temp_t);
    MinimalSignal_t1 = time(temp_t);
    [pks_MinimalSignal1,locs_MinimalSignal1,npks_MinimalSignal1,nlocs_MinimalSignal1] = findpeakdata(MinimalSignal_1,MinimalSignal_t1,sampling,10,0.01); 
    ref_MinimalSignal1=mean(pks_MinimalSignal1)-mean(npks_MinimalSignal1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% FrequencyResponse-sin2
    temp_t = temp_step(13)-10*sampling : temp_step(13)-5*sampling;
    FreqResSin_2 = ecg(temp_t);
    FreqResSin_t2 = time(temp_t);
    [pks_FreqResSin2,locs_FreqResSin2,npks_FreqResSin2,nlocs_FreqResSin2] = findpeakdata(FreqResSin_2,FreqResSin_t2,sampling,0.67); 
    ref_FreqResSin2=mean(pks_FreqResSin2)-mean(npks_FreqResSin2);
    %% FrequencyResponse-sin3
    temp_t = temp_step(14)-10*sampling : temp_step(14)-5*sampling;
    FreqResSin_3 = ecg(temp_t);
    FreqResSin_t3 = time(temp_t);
    [pks_FreqResSin3,locs_FreqResSin3,npks_FreqResSin3,nlocs_FreqResSin3] = findpeakdata(FreqResSin_3,FreqResSin_t3,sampling,1); 
    ref_FreqResSin3=mean(pks_FreqResSin3)-mean(npks_FreqResSin3);
    %% FrequencyResponse-sin4
    temp_t = temp_step(15)-10*sampling : temp_step(15)-5*sampling;
    FreqResSin_4 = ecg(temp_t);
    FreqResSin_t4 = time(temp_t);
    [pks_FreqResSin4,locs_FreqResSin4,npks_FreqResSin4,nlocs_FreqResSin4] = findpeakdata(FreqResSin_4,FreqResSin_t4,sampling,2); 
    ref_FreqResSin4=mean(pks_FreqResSin4)-mean(npks_FreqResSin4);
    %% FrequencyResponse-sin5
    temp_t = temp_step(16)-10*sampling : temp_step(16)-5*sampling;
    FreqResSin_5 = ecg(temp_t);
    FreqResSin_t5 = time(temp_t);
    [pks_FreqResSin5,locs_FreqResSin5,npks_FreqResSin5,nlocs_FreqResSin5] = findpeakdata(FreqResSin_5,FreqResSin_t5,sampling,5); 
    ref_FreqResSin5=mean(pks_FreqResSin5)-mean(npks_FreqResSin5);
    %% FrequencyResponse-sin6
    temp_t = temp_step(17)-10*sampling : temp_step(17)-5*sampling;
    FreqResSin_6 = ecg(temp_t);
    FreqResSin_t6 = time(temp_t);
    [pks_FreqResSin6,locs_FreqResSin6,npks_FreqResSin6,nlocs_FreqResSin6] = findpeakdata(FreqResSin_6,FreqResSin_t6,sampling,10); 
    ref_FreqResSin6=mean(pks_FreqResSin6)-mean(npks_FreqResSin6);
    %% FrequencyResponse-sin7
    temp_t = temp_step(18)-4*sampling : temp_step(18)-2*sampling;
    FreqResSin_7 = ecg(temp_t);
    FreqResSin_t7 = time(temp_t);
    [pks_FreqResSin7,locs_FreqResSin7,npks_FreqResSin7,nlocs_FreqResSin7] = findpeakdata(FreqResSin_7,FreqResSin_t7,sampling,20); 
    ref_FreqResSin7=mean(pks_FreqResSin7)-mean(npks_FreqResSin7);
    %% FrequencyResponse-sin8
    temp_t = temp_step(19)-4*sampling : temp_step(19)-2*sampling;
    FreqResSin_8 = ecg(temp_t);
    FreqResSin_t8 = time(temp_t);
    [pks_FreqResSin8,locs_FreqResSin8,npks_FreqResSin8,nlocs_FreqResSin8] = findpeakdata(FreqResSin_8,FreqResSin_t8,sampling,40); 
    ref_FreqResSin8=mean(pks_FreqResSin8)-mean(npks_FreqResSin8);


%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% DynamicRange
    h2=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    %DynamicRange1
    h2_1=subplot(311); 
    plot(Dynamic_t1, Dynamic_1);hold on;     text(Dynamic_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn1)); 
    ylabel('mV');xlabel('Time (s)');
    title('DynamicRange, Tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([Dynamic_t1(1) Dynamic_t1(length(Dynamic_t1)) -11 11]);grid on; grid(gca,'minor');
    %DynamicRange2
    h2_2=subplot(312); 
    plot(Dynamic_t2,Dynamic_2);hold on;      text(Dynamic_t2(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn2)); 
    ylabel('mV');xlabel('Time (s)');       
    title('DynamicRange, Tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=300, duration=40s, width=0');
    axis([Dynamic_t2(1) Dynamic_t2(length(Dynamic_t2)) -11 11]);grid on; grid(gca,'minor');
    %DynamicRange3
    h2_3=subplot(313); 
    plot(Dynamic_t3,Dynamic_3);hold on;       text(Dynamic_t3(1), 9 ,sprintf('pkpk: %.4f mV',ref_Dyn3)); 
    ylabel('mV');xlabel('Time (s)');
    title('DynamicRange, Tri, amp=10mV, f=10.4Hz, InputImped.=true, dcoffset=-300, duration=40s, width=0');
    axis([Dynamic_t3(1) Dynamic_t3(length(Dynamic_t3)) -11 11]);grid on; grid(gca,'minor');


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% InputImpedance
    h4=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    % InputImpedance1
    h4_1=subplot(411); 
    plot(InputImp_t1,InputImp_1);hold on;      text(InputImp_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp1)); 
    ylabel('mV');xlabel('Time (s)');
    title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([InputImp_t1(1) InputImp_t1(length(InputImp_t1)) -11 11]);grid on; grid(gca,'minor');
    % InputImpedance2
    h4_2=subplot(412); 
    plot(InputImp_t2,InputImp_2);hold on;          text(InputImp_t2(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp2)); 
    ylabel('mV');xlabel('Time (s)');
    title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=0, duration=40s, width=0');
    axis([InputImp_t2(1) InputImp_t2(length(InputImp_t2)) -11 11]);grid on; grid(gca,'minor');
    % InputImpedance3
    h4_3=subplot(413); 
    plot(InputImp_t3,InputImp_3);hold on;          text(InputImp_t3(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp3)); 
    ylabel('mV');xlabel('Time (s)');
    title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=300, duration=40s, width=0');
    axis([InputImp_t3(1) InputImp_t3(length(InputImp_t3)) -11 11]);grid on; grid(gca,'minor');
    % InputImpedance4
    h4_4=subplot(414); 
    plot(InputImp_t4,InputImp_4);hold on;           text(InputImp_t4(1), 9 ,sprintf('pkpk: %.4f mV',ref_InputImp4)); 
    ylabel('mV');xlabel('Time (s)');
    title('InputImpedance, Sin, amp=5mV, f=10Hz, InputImped.=false, dcoffset=-300, duration=40s, width=0');
    axis([InputImp_t4(1) InputImp_t4(length(InputImp_t4)) -11 11]);grid on; grid(gca,'minor');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    h5=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    % GainAccuracy1
    h5_1=subplot(311); 
    plot(GainAccuracy_t1,GainAccuracy_1);hold on;     text(GainAccuracy_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_GainAccuracy1)); 
    ylabel('mV');xlabel('Time (s)');
    title('GainAccuracy, Sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([GainAccuracy_t1(1) GainAccuracy_t1(length(GainAccuracy_t1)) -11 11]);grid on; grid(gca,'minor');
    % GainStability1
    h5_2=subplot(312); 
    plot(GainStability_t1,GainStability_1);hold on;    text(GainStability_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_GainStability1)); 
    ylabel('mV');xlabel('Time (s)');
    title('GainStability, Sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=150s, width=0');
    axis([GainStability_t1(1) GainStability_t1(length(GainStability_t1)) -11 11]);grid on; grid(gca,'minor');   
    % MinimalSignal
    h5_3=subplot(312); 
    plot(MinimalSignal_t1,MinimalSignal_1);hold on;    text(MinimalSignal_t1(1), 9 ,sprintf('pkpk: %.4f mV',ref_MinimalSignal1)); 
    ylabel('mV');xlabel('Time (s)');
    title('MinimalSignal, Sin, amp=0.05mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([MinimalSignal_t1(1) MinimalSignal_t1(length(MinimalSignal_t1)) -0.5 0.5]);grid on; grid(gca,'minor');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    h6=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);

    % FrequencyResponse-Sin2
    h6_2=subplot(311); 
    plot(FreqResSin_t2,FreqResSin_2);hold on;            text(FreqResSin_t2(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin2));
    ylabel('mV');xlabel('Time (s)');
    title('FrequencyResponse, Sin, amp=2mV, f=0.67Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([FreqResSin_t2(1) FreqResSin_t2(length(FreqResSin_t2)) -11 11]);grid on; grid(gca,'minor');
    % FrequencyResponse-Sin3
    h6_3=subplot(312); 
    plot(FreqResSin_t3,FreqResSin_3);hold on;              text(FreqResSin_t3(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin3));
    ylabel('mV');xlabel('Time (s)');
    title('FrequencyResponse, Sin, amp=2mV, f=1Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([FreqResSin_t3(1) FreqResSin_t3(length(FreqResSin_t3)) -11 11]);grid on; grid(gca,'minor');
    % FrequencyResponse-Sin4
    h6_4=subplot(313); 
    plot(FreqResSin_t4,FreqResSin_4);hold on;             text(FreqResSin_t4(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin4));
    ylabel('mV');xlabel('Time (s)');
    title('FrequencyResponse, Sin, amp=2mV, f=2Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([FreqResSin_t4(1) FreqResSin_t4(length(FreqResSin_t4)) -11 11]);grid on; grid(gca,'minor');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    h7=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    % FrequencyResponse-Sin5
    h7_1=subplot(311); 
    plot(FreqResSin_t5,FreqResSin_5);hold on;               text(FreqResSin_t5(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin5));
    ylabel('mV');xlabel('Time (s)');
    title('FrequencyResponse, Sin, amp=2mV, f=5Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([FreqResSin_t5(1) FreqResSin_t5(length(FreqResSin_t5)) -11 11]);grid on; grid(gca,'minor');
    % FrequencyResponse-Sin6
    h7_2=subplot(312); 
    plot(FreqResSin_t6,FreqResSin_6);hold on;               text(FreqResSin_t6(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin6));
    ylabel('mV');xlabel('Time (s)');
    title('FrequencyResponse, Sin, amp=2mV, f=10Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([FreqResSin_t6(1) FreqResSin_t6(length(FreqResSin_t6)) -11 11]);grid on; grid(gca,'minor');
    % FrequencyResponse-Sin7
    h7_3=subplot(313); 
    plot(FreqResSin_t7,FreqResSin_7);hold on;               text(FreqResSin_t7(1), 9 ,sprintf('pkpk: %.4f mV',ref_FreqResSin7));
    ylabel('mV');xlabel('Time (s)');
    title('FrequencyResponse, Sin, amp=2mV, f=20Hz, InputImped.=true, dcoffset=0, duration=40s, width=0');
    axis([FreqResSin_t7(1) FreqResSin_t7(length(FreqResSin_t7)) -11 11]);grid on; grid(gca,'minor');



    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% first page
    h1=figure('Visible', 'off');
    set(gcf,'PaperType','A4', 'paperunits','CENTIMETERS', 'PaperPosition',[0, 0, 20, 29]);
    
    subplot(3,1,1:2);
    x=1;d=0.03;
    text(-0.1, x ,sprintf('1. DynamicRange: Triangle, f=10.4Hz, Standard <= 10%%'),'FontWeight','bold'); x = x - d;
    
    text(-0.06, x ,sprintf('A. Amp=10mV, DC offset=0mV:'));text(0.45,x,sprintf('ref: %0.4f mV',ref_Dyn1));
    x = x - d;
    
    Qua=abs(((ref_Dyn2-ref_Dyn1)/ref_Dyn1)*100); if Qua<=10, str = 'Pass'; Item(1)=1; str_c ='blue'; else str = 'False';Item(1)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('B. Amp=10mV, DC offset=300mV:'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_Dyn2));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    Qua=abs(((ref_Dyn3-ref_Dyn1)/ref_Dyn1)*100); if Qua<=10, str = 'Pass'; Item(2)=1; str_c ='blue'; else str = 'False';Item(2)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('C. Amp=10mV, DC offset=-300mV:'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_Dyn3));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d-0.01;
    
    text(-0.1, x ,sprintf('2. Input Impedance: Sine wave, f=10Hz, Amp=5mV, Standard <= 6%%'),'FontWeight','bold'); x = x - d;
    text(-0.06, x ,sprintf('A. DC offset=0mV, InputImped=True :'));text(0.45,x,sprintf('ref: %0.4f mV',ref_InputImp1));
    x = x - d;
    
    Qua=abs(((ref_InputImp2-ref_InputImp1)/ref_InputImp1)*100); if Qua<=6, str = 'Pass'; Item(3)=1; str_c ='blue'; else str = 'False';Item(3)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('B. DC offset=0mV, InputImped=False :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_InputImp2));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    Qua=abs(((ref_InputImp3-ref_InputImp1)/ref_InputImp1)*100); if Qua<=6, str = 'Pass'; Item(4)=1; str_c ='blue'; else str = 'False';Item(4)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('C. DC offset=300mV, InputImped=False :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_InputImp3));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c);x = x - d;
    
    Qua=abs(((ref_InputImp4-ref_InputImp1)/ref_InputImp1)*100); if Qua<=6, str = 'Pass'; Item(5)=1; str_c ='blue'; else str = 'False';Item(5)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('D. DC offset=-300mV, InputImped=False :')); text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_InputImp4));
    text(0.65,x,sprintf('Var(pkpk-ref)= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d-0.01;
    
    text(-0.1, x ,sprintf('3. Gain Accuracy: Sine wave, f=5Hz, Amp=2mV, DC offset=0mV, Standard <= +/- 0.2mV'),'FontWeight','bold'); x = x - d;
    Qua = ref_GainAccuracy1 - 2; if abs(Qua)<=0.2, str = 'Pass'; Item(6)=1; str_c ='blue'; else str = 'False';Item(6)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('A. Peak: %0.4fmV,', ref_GainAccuracy1));text(0.45,x,sprintf('ref: %0.4f mV',ref_GainAccuracy1));
    text(0.65,x,sprintf('DifTo2mV:%0.4fmV,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d-0.01;

    text(-0.1, x ,sprintf('4. Gain Stability: Sine wave, f=5Hz, Amp=2mV, DC offset=0mV, Standard <= 3%%'),'FontWeight','bold'); x = x - d;
    Qua = abs(((ref_GainStability1 - ref_GainAccuracy1)/ref_GainAccuracy1)*100); if Qua<=3, str = 'Pass'; Item(7)=1; str_c ='blue'; else str = 'False';Item(7)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('A. At 2min Peak: %0.4fmV,',ref_GainStability1)); text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_GainStability1));
    text(0.65,x,sprintf('Var.2MinAgo:%0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d-0.01;    
    
    text(-0.1, x ,sprintf('5. Minimal Signal: Sine wave, f=10Hz, Amp=0.05mV, DC offset=0mV, Standard <= +/- 0.1mV'),'FontWeight','bold'); x = x - d;
    Qua = ref_MinimalSignal1 - 0.05 ; if abs(Qua)<=0.1, str = 'Pass'; Item(8)=1; str_c ='blue'; else str = 'False';Item(8)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('A. At Peak: %0.4fmV,',ref_MinimalSignal1)); text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_MinimalSignal1));
    text(0.65,x,sprintf('DifTo0.05mV:%0.4fmV,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d-0.01;
    
    text(-0.1, x ,sprintf('6. Frequency Response: b) Multi-Frequency Sine Wave, Amp=2mV, Standard:70%%~140%% '),'FontWeight','bold'); x = x - d;
        
    Qua = (ref_FreqResSin2/ref_FreqResSin5)*100; if (Qua<=140 && Qua>=70), str = 'Pass'; Item(9)=1; str_c ='blue'; else str = 'False';Item(9)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('A. f=0.67Hz :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_FreqResSin2));
    text(0.65,x,sprintf('Gain= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    Qua = (ref_FreqResSin3/ref_FreqResSin5)*100; if (Qua<=140 && Qua>=70), str = 'Pass'; Item(10)=1; str_c ='blue'; else str = 'False';Item(10)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('B. f=1.0Hz :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_FreqResSin3));
    text(0.65,x,sprintf('Gain= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    Qua = (ref_FreqResSin4/ref_FreqResSin5)*100; if (Qua<=140 && Qua>=70), str = 'Pass'; Item(11)=1; str_c ='blue'; else str = 'False';Item(11)=0; str_c = 'red'; end
    text(-0.06, x ,sprintf('C. f=2.0Hz :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_FreqResSin4));
    text(0.65,x,sprintf('Gain= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    text(-0.06, x ,sprintf('D. f=5.0Hz :'));text(0.45,x,sprintf('ref: %0.4f mV',ref_FreqResSin5));
    x = x - d;
    
    Qua = (ref_FreqResSin6/ref_FreqResSin5)*100; if (Qua<=140 && Qua>=70), str = 'Pass'; Item(12)=1; str_c ='blue'; else str = 'False';Item(12)=0; str_c = 'red'; end      
    text(-0.06, x ,sprintf('E. f=10Hz :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_FreqResSin6));
    text(0.65,x,sprintf('Gain= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    Qua = (ref_FreqResSin7/ref_FreqResSin5)*100; if (Qua<=140 && Qua>=70), str = 'Pass'; Item(13)=1; str_c ='blue'; else str = 'False';Item(13)=0; str_c = 'red'; end 
    text(-0.06, x ,sprintf('F. f=20Hz :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_FreqResSin7));
    text(0.65,x,sprintf('Gain= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    
    Qua = (ref_FreqResSin8/ref_FreqResSin5)*100; if (Qua<=140 && Qua>=70), str = 'Pass'; Item(14)=1; str_c ='blue'; else str = 'False';Item(14)=0; str_c = 'red'; end    
    text(-0.06, x ,sprintf('G. f=40Hz :'));text(0.45,x,sprintf('pkpk= %0.4f mV,',ref_FreqResSin8));
    text(0.65,x,sprintf('Gain= %0.2f%%,',Qua));
    text(0.9,x,'Outcome:'); text(1,x,sprintf('%s',str),'Color',str_c); x = x - d;
    axis off;
    
    text(-0.1, 1.13 ,sprintf('DxPatch PCB Performance Test: CH %s ID %s',CH,ID),'FontSize',16);
    text(-0.1, 1.10 ,sprintf('Report date: %s ',Date),'FontSize',8);
    text(-0.1, 1.08 ,sprintf('Firmware Version: %s ','K&Y001'),'FontSize',8);
    axis off;
    
    subplot(313);
    plot(time,ecg);hold on;
    ylabel('mV');xlabel('Time (s)');
    stem(time(temp_step),temp_y,'r');
    title('Signal graph:','FontWeight','bold','FontSize',16);grid on; grid(gca,'minor');
    axis([time(temp_step(1)) time(temp_step(18)) -11 11]);
  
    total = sum(Item);
%     
%     if show_indcator == true
%          plot(h2_1,locs_Dyn1, pks_Dyn1,'ro',nlocs_Dyn1,npks_Dyn1,'mo');
%          plot(h2_2,locs_Dyn2, pks_Dyn2,'ro',nlocs_Dyn2,npks_Dyn2,'mo'); 
%          plot(h2_3,locs_Dyn3, pks_Dyn3,'ro',nlocs_Dyn3,npks_Dyn3,'mo'); 
%          plot(h3_1,locs_Dyn4, pks_Dyn4,'ro',nlocs_Dyn4,npks_Dyn4,'mo');
%          plot(h3_2,locs_Dyn5, pks_Dyn5,'ro',nlocs_Dyn5,npks_Dyn5,'mo');
%          plot(h3_3,locs_Dyn6, pks_Dyn6,'ro',nlocs_Dyn6,npks_Dyn6,'mo');
%          plot(h4_1,locs_InputImp1, pks_InputImp1,'ro',nlocs_InputImp1,npks_InputImp1,'mo'); 
%          plot(h4_2,locs_InputImp2, pks_InputImp2,'ro',nlocs_InputImp2,npks_InputImp2,'mo');
%          plot(h4_3,locs_InputImp3, pks_InputImp3,'ro',nlocs_InputImp3,npks_InputImp3,'mo'); 
%          plot(h4_4,locs_InputImp4, pks_InputImp4,'ro',nlocs_InputImp4,npks_InputImp4,'mo');
%          plot(h5_1,locs_GainAccuracy1, pks_GainAccuracy1,'ro',nlocs_GainAccuracy1,npks_GainAccuracy1,'mo');  
%          plot(h5_2,locs_GainStability1, pks_GainStability1,'ro',nlocs_GainStability1,npks_GainStability1,'mo');
%          plot(h5_3,slope_line,slope_v,'g');plot(h5_3,i_before, v_before,'r', i_after, v_after,'m');
%          plot(h6_1,locs_FreqResSin1, pks_FreqResSin1,'ro',nlocs_FreqResSin1,npks_FreqResSin1,'mo'); 
%          plot(h6_2,locs_FreqResSin2, pks_FreqResSin2,'ro',nlocs_FreqResSin2,npks_FreqResSin2,'mo'); 
%          plot(h6_3,locs_FreqResSin3, pks_FreqResSin3,'ro',nlocs_FreqResSin3,npks_FreqResSin3,'mo');
%          plot(h6_4,locs_FreqResSin4, pks_FreqResSin4,'ro',nlocs_FreqResSin4,npks_FreqResSin4,'mo');  
%          plot(h7_1,locs_FreqResSin5, pks_FreqResSin5,'ro',nlocs_FreqResSin5,npks_FreqResSin5,'mo'); 
%          plot(h7_2,locs_FreqResSin6, pks_FreqResSin6,'ro',nlocs_FreqResSin6,npks_FreqResSin6,'mo'); 
%          plot(h7_3,locs_FreqResSin7, pks_FreqResSin7,'ro',nlocs_FreqResSin7,npks_FreqResSin7,'mo'); 
%          plot(h7_4,locs_FreqResSin8, pks_FreqResSin8,'ro',nlocs_FreqResSin8,npks_FreqResSin8,'mo'); 
%          plot(h8_1,locs_FreqResSin9, pks_FreqResSin9,'ro',nlocs_FreqResSin9,npks_FreqResSin9,'mo');
%          plot(h8_2,locs_FreqResTri1, pks_FreqResTri1,'ro',i_base1,v_base1,'m'); 
%          plot(h8_3,locs_FreqResTri2, pks_FreqResTri2,'ro',i_base2,v_base2,'m'); 
%     end
    
    
    if total==14, str1 = 'Pass'; color = 'g';  else str1 = 'FAIL'; color = 'r'; end
    str = sprintf( '結果: %s',str1);
    
    ResultFilename = ['CH_'  CH  '_ID_' ID '_' Date '_' str1 '.pdf'];
    print(h1, '-dpdf',ResultFilename);
    if print_all_page == true
        print(h2, '-dpdf','Page1.pdf');
        print(h4, '-dpdf','Page2.pdf');
        print(h5, '-dpdf','Page3.pdf');
        print(h6, '-dpdf','Page4.pdf');
        print(h7, '-dpdf','Page5.pdf');

    end
    
    hm=msgbox(str,'分析完成');
    set(hm, 'units', 'normalized', 'position', [0.4 0.5 0.4 0.2]); %makes box bigger
    ah = get( hm, 'CurrentAxes' );
    ch = get( ah, 'Children' );
    set( ch, 'FontSize', 80 ,'Color',color); %makes text bigger
end

function [pks,locs,npks,nlocs]=findpeakdata(data,time,sampling,Hz,p)
    if nargin <5
        p=0.1;  
        m=0;
    else
        temp=mean(data);
        data=data-temp;
        m=1;
    end
    
    duration = ceil((sampling/Hz)/2);
    [pks,locs] = findpeaks(data,'MinPeakDistance',duration,'MINPEAKHEIGHT',p); 
    [npks,nlocs] = findpeaks(-data,'MinPeakDistance',duration,'MINPEAKHEIGHT',p); 
    
    if locs(1)<nlocs(1)
        locs(1)=[];pks(1)=[];
    else
        nlocs(1)=[];npks(1)=[];
    end
    
    if locs(length(locs))>nlocs(length(nlocs))
        locs(length(locs))=[]; pks(length(locs))=[];
    else
        nlocs(length(nlocs))=[];npks(length(nlocs))=[];
    end
    
    npks = -npks;
    if m==1
        npks = npks + temp;
        pks = pks + temp;
    end

    locs = time(locs); 
    nlocs = time(nlocs);
end

function   [v_before,i_before,v_after,i_after,slope_line,slope_v,slope] = findbaseline(data,time)
    d1 = diff(data);
    [~,locs] = findpeaks(d1,'MinPeakDistance',1,'MINPEAKHEIGHT',0.5);
    [~,nlocs] = findpeaks(-d1,'MinPeakDistance',1,'MINPEAKHEIGHT',0.5);   
    
    i_before=time(locs(1)-255:locs(1)-5);
    v_before=data(locs(1)-255:locs(1)-5);
    i_after=time(nlocs(1)+5:nlocs(1)+255);
    v_after=data(nlocs(1)+5:nlocs(1)+255);
    
    slope_line=time(nlocs(1)+5:locs(2)-5);
    slope_v=data(nlocs(1)+5:locs(2)-5);
    
    slope = (data(locs(2)-5)-data(nlocs(1)+5)) / (time(locs(2)-5)-data(nlocs(1)-5));
    
end

function  [pks,locs,baseline_v,baseline_i] = findbaseline2(data,time,sampling)
    [pks,locs] = findpeaks(data,'MinPeakDistance',sampling/10,'MINPEAKHEIGHT',0.3);
    pks=pks(2);locs=locs(2);
    baseline_i = time(locs+sampling/4:locs+sampling/2);
    baseline_v = data(locs+sampling/4:locs+sampling/2);
    locs = time(locs);
end
