﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Reflection;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Drawing;
using PdfSharp.Pdf;

namespace DxPatchPCBTestApp
{
    public class SignalTestReport
    {
        public SignalTestResultHolder TestResult { get; private set; }
        public XPoint[] DataPoints { get; private set; }
        public Bitmap ChartBitmap { get; set; } = null;

        private string AppVersion { get; set; } = "";
        private DateTime ReportDateTime { get; set; }
        private string PPID { get; set; } = "";
        private string FirmwareVersion { get; set; } = "";
        private string[][] TableData { get; set; }
        private string CandidateFilePath { get; set; } = "";
        private string TestscriptFilePath { get; set; } = "";

        public SignalTestReport(SignalTestResultHolder testResult, DateTime reportDateTime, string ppid, 
            string firmwareVersion, string[][] tableData, System.Drawing.Point[] signalPoints, 
            string candidateFilePath = "", string testscriptFilePath = "")
        {
            TestResult = testResult;
            AppVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            ReportDateTime = reportDateTime;
            PPID = ppid;
            FirmwareVersion = firmwareVersion;
            TableData = tableData;
            DataPoints = PDFChart.ConvertToXPoints(signalPoints);
            CandidateFilePath = candidateFilePath;
            TestscriptFilePath = testscriptFilePath;
        }

        public void AppendToBody(string content)
        {
            //m_body += content;
        }

        public string GetHTML()
        {
            string htmlHead = "<style>"
               + "body { font-family: Arial; font-size: 10pt; max-width: 800px; } "
               + "h1 { font-size: 14pt; } "
               + "#appVersion { float:right; } "
               + "table { width: 100%; border-collapse:collapse;  margin:20px 0 20px 0; } "
               + "th { border: 1px solid grey; padding: 5px; } "
               + "td { border: 1px solid grey; padding: 5px; font-size: 10pt } "
               + "td.col1 { width: 80%; border: 1px solid grey; text-align: left; } "
               + "td.col2 { width: 20%; text-align: right; } </style>";

            string htmlBody = "<h1>DxPatch PCB Performance Test</h1>"
               + "<div id='appVersion'>Application version: " + AppVersion + "</div>"
               + "Test Date: " + ReportDateTime.ToString("yyyy-MM-dd hh:mm:ss")
               + ".  PPID: " + PPID
               + ".  Firmware version: " + FirmwareVersion;

            htmlBody += "<table>";
            for (int i = 0; i < TableData.Length; i++)
            {
                //New row
                htmlBody += "<tr>";
                for (int j = 0; j < TableData[i].Length; j++)
                {
                    htmlBody += "<td>" + TableData[i][j] + "</td>";
                }
                htmlBody += "</tr>";
            }
            htmlBody += "</table>";

            return "<html><head>" + htmlHead + "</head><body>" + htmlBody + "</body></html>";
        }

        public void SaveHTML(string fileName)
        {
            try
            {
                File.WriteAllText(fileName, GetHTML());
            }
            catch (Exception ex)
            {
                AppLog.Write("Error saving test report.  " + ex.Message);
                throw;
            }
        }

        public void SavePDF(string fileName)
        {
            Document mDoc = new Document();

            //Use PDFSharp classes to draw signal chart directly onto PDF
            PdfDocument pdfDoc = new PdfDocument();
            PdfPage page = pdfDoc.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);

            //With MigraDoc, we don’t add pages, we add sections(at least one):
            Section section = mDoc.AddSection();

            section.PageSetup.TopMargin = 40;
            section.PageSetup.LeftMargin = 30;
            section.PageSetup.RightMargin = 30;
            section.PageSetup.BottomMargin = 30;

            //Header
            Paragraph titleParagraph = new Paragraph();
            titleParagraph.Format.Font.Size = 16;
            titleParagraph.AddFormattedText("DxPatch PCB Performance Test", TextFormat.Bold);

            Paragraph p1 = new Paragraph();
            p1.Format.Font.Size = 10;
            p1.Format.Alignment = ParagraphAlignment.Right;
            p1.AddText("Report date: " + ReportDateTime.ToString("yyyy-MM-dd HH:mm:ss"));
            p1.Format.LineSpacing = 0;

            Paragraph p2 = new Paragraph();
            p2.Format.Font.Size = 10;
            p2.Format.Alignment = ParagraphAlignment.Right;
            p2.AddText("Test software version: " + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion);
            p2.Format.LineSpacing = 0;

            Table tblHeader = MakePDFTable(1, 2, "HEADER", border: false);
            tblHeader.Columns[0].Width = 300;
            tblHeader.Columns[1].Width = 235;
            tblHeader.Rows[0].Cells[0].Add(titleParagraph);
            tblHeader.Rows[0].Cells[1].Add(p1);
            tblHeader.Rows[0].Cells[1].Add(p2);
            section.Add(tblHeader);

            section.AddParagraph();

            Table tblTestInfo = MakePDFTable(5, 2, "TESTINFO", border: false);
            tblTestInfo.Columns[0].Width = 100;
            tblTestInfo.Columns[1].Width = 435;

            tblTestInfo.Rows[0].Cells[0].AddParagraph("PPID: ");
            tblTestInfo.Rows[0].Cells[1].AddParagraph(PPID);

            tblTestInfo.Rows[1].Cells[0].AddParagraph("Firmware version: ");
            tblTestInfo.Rows[1].Cells[1].AddParagraph(FirmwareVersion);

            tblTestInfo.Rows[2].Cells[0].AddParagraph("Recording date: ");
            tblTestInfo.Rows[2].Cells[1].AddParagraph(TestResult.CandidateFileStartDT.ToString("yyyy-MM-dd HH:mm:ss"));

            const int paragraphWidth = 75;
            tblTestInfo.Rows[3].Cells[0].AddParagraph("Candidate file: ");
            if(CandidateFilePath.Length < paragraphWidth) {
                tblTestInfo.Rows[3].Cells[1].AddParagraph(CandidateFilePath);
            } else {
                tblTestInfo.Rows[3].Cells[1].AddParagraph(CandidateFilePath.Substring(0, paragraphWidth));
                tblTestInfo.Rows[3].Cells[1].AddParagraph(CandidateFilePath.Substring(paragraphWidth));
            }

            tblTestInfo.Rows[4].Cells[0].AddParagraph("Test script file: ");
            if(TestscriptFilePath.Length < paragraphWidth)
            {
                tblTestInfo.Rows[4].Cells[1].AddParagraph(TestscriptFilePath);
            } else {
                tblTestInfo.Rows[4].Cells[1].AddParagraph(TestscriptFilePath.Substring(0, paragraphWidth));
                tblTestInfo.Rows[4].Cells[1].AddParagraph(TestscriptFilePath.Substring(paragraphWidth));
            }

            section.Add(tblTestInfo);

            section.AddParagraph();

            Paragraph pTableTitle = new Paragraph();
            pTableTitle.Format.Font.Size = 14;
            pTableTitle.AddFormattedText("Test results:", TextFormat.Bold);
            section.Add(pTableTitle);
            section.AddParagraph();

            Table tblSignalTests = MakePDFTable(TableData, new int[] { 75, 105, 75, 75, 75, 45, 65 }, "DATATABLE", prependRowNums: true);
            section.Add(tblSignalTests);

            section.AddParagraph();

            Paragraph pChartTitle = new Paragraph();
            pChartTitle.Format.Font.Size = 14;
            pChartTitle.AddFormattedText("Signal graph:", TextFormat.Bold);
            section.Add(pChartTitle);
            section.AddParagraph();

            section.AddParagraph("Candidate file total number of samples: " + TestResult.CandidateFileNumSamples);
            section.AddParagraph("Found start marker at index: " + TestResult.CandidateFileStartIndex);

            Paragraph pChartPosition = new Paragraph();
            pChartPosition.Tag = "CHART";
            section.Add(pChartPosition);

            DocumentRenderer mDocRenderer = new DocumentRenderer(mDoc);
            mDocRenderer.PrepareDocument();

            RenderInfo[] renderInfos = mDocRenderer.GetRenderInfoFromPage(1);

            double chartX = 0;
            double chartY = 0;
            double chartWidth = 0;

            foreach (var item in renderInfos)
            {
                DocumentObject docObj = item.DocumentObject;

                if (item.DocumentObject.Tag != null && item.DocumentObject.Tag.ToString() == "DATATABLE") {
                    chartX = item.LayoutInfo.ContentArea.X;
                    chartWidth = item.LayoutInfo.ContentArea.Width;
                }

                if (item.DocumentObject.Tag != null && item.DocumentObject.Tag.ToString() == "CHART") {
                    chartY = item.LayoutInfo.ContentArea.Y + item.LayoutInfo.ContentArea.Height;
                }
            }

            PDFTestResultChart signalChart = new PDFTestResultChart(DataPoints, TestResult, chartX, chartY, chartWidth);
            signalChart.Draw(gfx);

            mDocRenderer.RenderPage(gfx, 1);

            if (!fileName.Contains(".pdf")) fileName += ".pdf";

            try {
                pdfDoc.Save(fileName);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        private Table MakePDFTable(int numRows, int numColumns, string tag, bool border = true)
        {
            Table table = new Table();
            table.Tag = tag;
            table.LeftPadding = 2;
            table.TopPadding = 2;
            table.RightPadding = 2;
            table.BottomPadding = 2;
            if(border) table.Borders.Width = 0.5;

            for (int i = 0; i < numColumns; i++) {
                table.AddColumn();
            }

            for (int i = 0; i < numRows; i++)
            {
                table.AddRow();
            }

            for (int i = 0; i < numRows; i++)
            {
                Cell cell = new Cell();
                table.Rows[i].Cells.Add(cell);
            }
            
            return table;
        }

        private Table MakePDFTable(string[][] tableData, int[] colWidths, string tag, bool prependRowNums = false)
        {
            Table table = new Table();
            table.Tag = tag;
            table.LeftPadding = 2;
            table.TopPadding = 2;
            table.RightPadding = 2;
            table.BottomPadding = 2;
            table.Borders.Width = 0.5;

            //Calculate number of columns
            int maxColumns = 0;
            foreach (string[] dataRow in tableData) {
                if (dataRow.Length > maxColumns) maxColumns = dataRow.Length;
            }

            if (prependRowNums)
            {
                Column col = table.AddColumn();
                col.Width = 20;
            }

            for (int i = 0; i < maxColumns; i++)
            {
                Column col = table.AddColumn();
                if (colWidths.Length >= i + 1) col.Width = colWidths[i];
            }

            //Add rows with data
            for (int i = 0; i < tableData.Length; i++)
            {
                Row row = table.AddRow();

                if (prependRowNums)
                {
                    Cell c1 = row.Cells[0];
                    c1.Format.Font.Size = 8;
                    c1.AddParagraph((i + 1).ToString());
                }

                for (int j = 0; j < tableData[i].Length; j++)
                {
                    Cell cell = new Cell();
                    cell.Format.Font.Size = 8;
                    cell.AddParagraph(tableData[i][j]);
                    row.Cells.Add(cell);
                }
            }

            return table;
        }

        private Bitmap ScaleImage(Bitmap inputImage, float targetWidth, float targetHeight)
        {
            //Target sizing (scale factor):
            float scale = targetWidth / inputImage.Width;
            var scaleWidth = (int)(inputImage.Width * scale);
            var scaleHeight = (int)(inputImage.Height * scale);

            var bmp = new Bitmap(scaleWidth, scaleHeight);

            using(Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = InterpolationMode.High;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.AntiAlias;

                g.FillRectangle(Brushes.Black, new RectangleF(0, 0, bmp.Width, bmp.Height));
                g.DrawImage(inputImage, new Rectangle(0, 0, bmp.Width, bmp.Height));
            }

            return bmp;
        }
    }

    public class PDFTestResultChart : PDFChart
    {
        public PDFTestResultChart(XPoint[] points, SignalTestResultHolder testResult, double xOffset, double yOffset, double chartWidth = 800, int chartHeight = 800)
            : base(points, xOffset, yOffset, chartWidth, chartHeight)
        {
            //Add start marker annotation
            Annotation startMarker = new Annotation();
            startMarker.Line = new LineAnnotation
            {
                X1 = testResult.CandidateFileStartIndex,
                Y1 = 10,
                X2 = testResult.CandidateFileStartIndex,
                Y2 = this.ChartHeight
            };
            this.Annotations.Add(startMarker);

            //Add signal tests annotation
            for (int i = 0; i < testResult.Results.Length; i++)
            {
                SignalTestResult sigResult = testResult.Results[i];
                if (sigResult.SampleChunk == null) continue;
                int sampleIndex = sigResult.SampleChunk.StartIndex;
                string resultText = (i + 1).ToString();

                Annotation ann = new Annotation
                {
                    Text = new TextAnnotation { Text = resultText, X = sampleIndex + 10, Y = 10 },
                    Line = new LineAnnotation { X1 = sampleIndex, Y1 = 0, X2 = sampleIndex, Y2 = this.ChartHeight }
                };

                this.Annotations.Add(ann);
            }
        }
    }

    public class PDFChart
    {
        public XPoint[] DataPoints { get; private set; }
        public bool ShowMajorGrid { get; set; } = true;
        public bool ShowMinorGrid { get; set; } = false;
        public double ChartWidth { get; private set; } = 500;
        public double ChartHeight { get; private set; } = 300;
        public double XOffset { get; private set; } = 0;
        public double YOffset { get; private set; } = 0;

        private int m_xAxisMinorUnit = 500;
        private int m_xAxisMajorUnit = 2500;
        private int m_yAxisMinorUnit = 20;
        private int m_yAxisMajorUnit = 200;
        private XPen m_borderPen = new XPen(XColor.FromKnownColor(XKnownColor.Black), 0.5);
        private XPen m_axisPen = new XPen(XColor.FromKnownColor(XKnownColor.LightGray), 0.5);
        private XPen m_dataPen = new XPen(XColor.FromKnownColor(XKnownColor.Black), 0.5);
        private XPen m_lineAnnotationPen = new XPen(XColor.FromKnownColor(XKnownColor.Blue), 1);
        public List<Annotation> Annotations { get; } = new List<Annotation>();

        public PDFChart(XPoint[] points, double xOffset, double yOffset, double chartWidth = 500, int chartHeight = 300)
        {
            XOffset = xOffset;
            YOffset = yOffset;
            ChartWidth = chartWidth;
            DataPoints = points;
            m_lineAnnotationPen.DashPattern = new double[] { 5, 5 };
        }

        public PDFChart(System.Drawing.Point[] points, double xOffset, double yOffset, int chartWidth = 500, int chartHeight = 300)
        {
            XOffset = xOffset;
            YOffset = yOffset;
            ChartWidth = chartWidth;
            DataPoints = ConvertToXPoints(points);
            m_lineAnnotationPen.DashPattern = new double[] { 5, 5 };
        }

        public static XPoint[] ConvertToXPoints(System.Drawing.Point[] points)
        {
            //Convert the points to XPoints
            List<XPoint> xPointList = new List<XPoint>();
            foreach (var p in points)
            {
                xPointList.Add(new XPoint(p.X, p.Y));
            }
            return xPointList.ToArray();
        }

        private XPoint[] OffsetPoints(XPoint[] points, double xOffset, double yOffset)
        {
            for (int i = 0; i < points.Length; i++)
            {
                XPoint p = points[i];
                p.X += xOffset;
                p.Y += yOffset;
                points[i] = p;
            }

            return points;
        }

        public void Draw(XGraphics g)
        {
            DrawGrid(g);
            DrawDataSeries(g, DataPoints);
            DrawResultsAnnotation(g);
            DrawBorder(g);
        }

        private void DrawGrid(XGraphics g)
        {
            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            double xAxisMinorUnitScaled = m_xAxisMinorUnit * sf.ScaleFactorX;
            double xAxisMajorUnitScaled = m_xAxisMajorUnit * sf.ScaleFactorX;
            double yAxisMinorUnitScaled = m_yAxisMinorUnit * sf.ScaleFactorY;
            double yAxisMajorUnitScaled = m_yAxisMajorUnit * sf.ScaleFactorY;

            double maxWidth = ChartWidth + XOffset;
            double maxHeight = ChartHeight + YOffset;

            //VERTICAL MINOR lines but don't draw lines if will be too small */
            if (ShowMinorGrid && xAxisMinorUnitScaled >= 5)
            {
                for (double x = XOffset; x < maxWidth; x += Math.Round(xAxisMinorUnitScaled))
                    g.DrawLine(m_axisPen, x, YOffset, x, maxHeight);
            }

            //VERTICAL MAJOR lines but don't draw lines if will be too small */
            if (ShowMajorGrid && xAxisMajorUnitScaled >= 5)
            {
                for (double x = XOffset; x < maxWidth; x += Math.Round(xAxisMajorUnitScaled))
                    g.DrawLine(m_axisPen, x, YOffset, x, maxHeight);
            }

            //HORIZONTAL MINOR lines but don't draw lines if will be too small */
            if (ShowMinorGrid && yAxisMinorUnitScaled >= 5)
            {
                for (double y = YOffset; y < maxHeight; y += Math.Round(yAxisMinorUnitScaled))
                    g.DrawLine(m_axisPen, XOffset, y, maxWidth, y);
            }

            //HORIZONTAL MAJOR lines but don't draw lines if will be too small */
            if (ShowMajorGrid && yAxisMajorUnitScaled >= 5)
            {
                for (double y = YOffset; y < maxHeight; y += Math.Round(yAxisMajorUnitScaled))
                    g.DrawLine(m_axisPen, XOffset, y, maxWidth, y);
            }
        }

        private void DrawDataSeries(XGraphics g, XPoint[] points)
        {
            points = ScalePoints(points, XOffset, YOffset);

            //Draw the data points and connecting lines
            if (points.Length >= 1)
            {
                g.SmoothingMode = XSmoothingMode.AntiAlias;
                g.DrawLines(m_dataPen, points);
            }
        }

        public void DrawResultsAnnotation(XGraphics g)
        {
            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            if (Annotations != null)
            {
                foreach (Annotation a in Annotations)
                {
                    g.DrawLine(
                        m_lineAnnotationPen,
                        XOffset + a.Line.X1 * sf.ScaleFactorX,
                        YOffset + a.Line.Y1,
                        XOffset + a.Line.X2 * sf.ScaleFactorX,
                        YOffset + a.Line.Y2
                    );

                    if (a.Text.Text != null)
                    {
                        double xScaled = XOffset + a.Text.X * sf.ScaleFactorX;
                        g.DrawString(a.Text.Text, new XFont("Arial", 10), XBrushes.Black, new XPoint(xScaled, YOffset + a.Text.Y));
                    }
                }
            }
        }

        private void DrawBorder(XGraphics g)
        {
            g.DrawRectangle(m_borderPen, new XRect(XOffset, YOffset, ChartWidth, ChartHeight));
        }

        private XPoint[] ScalePoints(XPoint[] points, double xOffset, double yOffset)
        {
            var scaledPoints = new List<XPoint>();

            ScaleFactor sf = CalculateScaleFactors(DataPoints, ChartWidth, ChartHeight);

            for (int i = 0; i < points.Length; i++)
            {
                double scaledY = ChartHeight / 2 + -1 * Math.Round(points[i].Y * sf.ScaleFactorY);
                double scaledX = points[i].X * sf.ScaleFactorX;
                XPoint modifiedPoint = new XPoint(xOffset + scaledX, yOffset + scaledY);
                scaledPoints.Add(modifiedPoint);
            }

            return scaledPoints.ToArray();
        }

        public static ScaleFactor CalculateScaleFactors(XPoint[] dataPoints, double chartWidth, double chartHeight)
        {
            MinMax m = GetMinMax(dataPoints);

            ScaleFactor sf = new ScaleFactor();

            //Calculate x scale factor
            sf.ScaleFactorX = chartWidth / (m.MaxX - m.MinX);

            //Calculate y scale factor
            double yMax = (Math.Abs(m.MinY) > m.MaxY) ? Math.Abs(m.MinY) : m.MaxY;
            sf.ScaleFactorY = (chartHeight / 2) / yMax;

            return sf;
        }

        public static MinMax GetMinMax(XPoint[] points)
        {
            MinMax minMax = new MinMax();

            if (points == null || points.Length <= 0) return minMax;

            double yMin = 1000000; double yMax = 0;
            double xMin = 1000000; double xMax = 0;

            foreach (XPoint p in points)
            {
                if (p.X < xMin) xMin = p.X;
                if (p.X > xMax) xMax = p.X;
                if (p.Y < yMin) yMin = p.Y;
                if (p.Y > yMax) yMax = p.Y;
            }

            minMax.MinX = xMin;
            minMax.MaxX = xMax;
            minMax.MinY = yMin;
            minMax.MaxY = yMax;

            return minMax;
        }

        public struct MinMax
        {
            public double MinX;
            public double MaxX;
            public double MinY;
            public double MaxY;
        }

        public struct ScaleFactor
        {
            public double ScaleFactorX;
            public double ScaleFactorY;
        }

        public struct TextAnnotation
        {
            public double X;
            public double Y;
            public string Text;
        }

        public struct LineAnnotation
        {
            public double X1;
            public double Y1;
            public double X2;
            public double Y2;
        }

        public class Annotation
        {
            public LineAnnotation Line { get; set; }
            public TextAnnotation Text { get; set; }
            public Annotation() { }
        }
    }
}
