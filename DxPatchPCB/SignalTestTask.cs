﻿using System;
using System.Xml;
using System.Globalization;
using WhaleTeqSECG_SDK;

namespace DxPatchPCBTestApp
{
    public class SignalTestTask
    {
        public string action = "";
        public SignalTestFunc testFunc;
        public OutputLead_E outputLead;
        public OutputFunction outputFunc;
        public double amplitude;
        public double frequency;
        public DateTime startTime;
        public int duration;
        public bool inputImpedance;
        //YT Shih
        public int width;


        public SignalTestTask() { }

        public SignalTestTask(XmlAttributeCollection xmlAttr)
        {
            if (xmlAttr["testFunc"] != null) {
                foreach (SignalTestFunc f in Enum.GetValues(typeof(SignalTestFunc))) {
                    if (f.ToString().ToUpper() == xmlAttr["testFunc"].Value.ToUpper()) {
                        this.testFunc = f;
                        break;
                    }
                }
            }

            if (xmlAttr["inputImpedance"] != null)
                this.inputImpedance = Boolean.Parse(xmlAttr["inputImpedance"].Value);

            if (xmlAttr["outputLead"] != null)
                this.outputLead = (OutputLead_E)Enum.Parse(
                    typeof(OutputLead_E), xmlAttr["outputLead"].Value, true);

            if(xmlAttr["amplitude"] != null)
                this.amplitude = Double.Parse(xmlAttr["amplitude"].Value);

            if (xmlAttr["frequency"] != null)
                this.frequency = Double.Parse(xmlAttr["frequency"].Value);

            if (xmlAttr["startTime"] != null) {
                DateTime.TryParseExact(
                    xmlAttr["startTime"].Value,
                    "yyyy-MM-dd hh:mm:ss.fff",
                    CultureInfo.InvariantCulture, 
                    DateTimeStyles.None, out this.startTime);
            }

            if (xmlAttr["duration"] != null)
                this.duration = Int32.Parse(xmlAttr["duration"].Value);

            if (xmlAttr["width"] != null)
                this.width = Int32.Parse(xmlAttr["width"].Value);
        }
        
        public TimeSpan durationTS() {
            return TimeSpan.FromMilliseconds(duration);
        }
    }
}
