﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.IO;
using WhaleTeqSECG_SDK;

namespace DxPatchPCBTestApp
{
    public enum SignalTestFunc {
        Marker, DynamicRange, InputImpedance, GainAccuracy, FrequencyResponse, GainStability
    }

    public class SignalTestResultHolder
    {
        private List<SignalTestResult> m_results = new List<SignalTestResult>();
        public int CandidateFileNumSamples { get; set; } = -1;
        public int CandidateFileStartIndex { get; set; } = -1;
        public DateTime CandidateFileStartDT { get; set; }

        public SignalTestResult[] Results {
            get {
                return m_results.ToArray();
            }
        }

        public SignalTestResultHolder() { }

        public void AddResult(SignalTestResult result)
        {
            m_results.Add(result);
        }

        public bool IsPass()
        {
            bool result = true;
            foreach (SignalTestResult r in Results) {
                if (!r.IsPass) { result = false; break; }
            }
            return result;
        }
    }

    public class SignalTester
    {
        private Queue<SignalTestTask> m_taskQueue = new Queue<SignalTestTask>();
        private List<SignalTestTask> m_taskList = new List<SignalTestTask>();
        private List<string[]> m_reportData = new List<string[]>();
        private short[] m_candidateData;
        private double m_rawSampleRate;
        private double m_chanResolution;

        public DateTime CandidateFileStartDT { get; private set; }
        public int CandidateFileStartIndex { get; private set; } = -1;

        public string[][] ReportData {
            get {
                return m_reportData.ToArray();
            }
        }

        public SignalTester(string scriptFilePath, string candidateFilePath, double fileSampleRate, double resolutionRA)
        {
            m_rawSampleRate = fileSampleRate;
            m_chanResolution = resolutionRA;

            m_taskList = DataTools.LoadSignalTestsFromXml(scriptFilePath);

            if (Path.GetExtension(candidateFilePath).ToUpper() == ".RAW") {
                m_candidateData = DataTools.GetHemoData(candidateFilePath, 1, true);
            }

            CandidateFileStartIndex = FindCandidateStart(m_candidateData);
            TimeSpan timeDiffFromStart = TimeSpan.FromSeconds(CandidateFileStartIndex / m_rawSampleRate);
            CandidateFileStartDT = m_taskList[0].startTime.Subtract(timeDiffFromStart);
        }

        private void AddReportData(string[] dataRow) {
            m_reportData.Add(dataRow);
        }

        public string GetReportText()
        {
            string details = "<br/>Candidate file total samples: " + m_candidateData.Length
                + ", start time: " + CandidateFileStartDT.ToString(AppConfig.DateTimeFormat)
                + ", duration: " + (m_candidateData.Length / m_rawSampleRate).ToString("0.0") + " seconds.";

            details += "<br/>Found candidate start index at: " + CandidateFileStartIndex;

            return  details;
        }

        private int FindCandidateStart(short[] sampleData)
        {
            int position = 10;
            bool found = false;
            int attemptCount = 0;
            const int MAX_ATTEMPTS = 20;

            while(!found && attemptCount < MAX_ATTEMPTS)
            {
                position = FindJumpIndex(sampleData, startIndex: position, jumpSize: 700);
                
                if (position >= 0 && position < sampleData.Length - 1)
                {
                    int valAtJump = sampleData[position];
                    //Should be a short plateau after the jump
                    for (int i = position + 10; i <= position + 50 && i < sampleData.Length; i++)
                    {
                        if (i >= sampleData.Length) { found = false; break; }
                        int diff = Math.Abs(valAtJump - sampleData[i]);
                        if (diff <= 20) { found = true; }
                        else { found = false; break; }
                    }
                }
                else
                {
                    break; //could not find signal jump point.
                }
                attemptCount++;
            }

            return found ? position : -1;
        }

        private int FindJumpIndex(short[] sampleData, int startIndex, int jumpSize)
        {
            int jumpIndex = -1;
            bool found = false;
            int minY = 0;
            int maxY = 0;

            if (startIndex >= sampleData.Length) return -1;

            if(sampleData.Length >= 1) {
                minY = sampleData[startIndex];
                maxY = sampleData[startIndex];
            }

            for (int i = startIndex; i < sampleData.Length; i++) {
                short s = sampleData[i];

                if (s > maxY && Math.Abs(s - maxY) > jumpSize)
                { found = true; } else { maxY = s; }

                if (s < minY && Math.Abs(s - minY) > jumpSize)
                { found = true; } else { minY = s; }

                if (found) { jumpIndex = i; break; }
            }

            //After jump find peak or plateau
            while (jumpIndex >= 0 && jumpIndex < sampleData.Length - 2)
            {
                int a = Math.Abs(sampleData[jumpIndex - 1]);
                int b = Math.Abs(sampleData[jumpIndex]);
                int c = Math.Abs(sampleData[jumpIndex + 1]);

                if (b > a && b >= c) break; //Found peak or plateau
                jumpIndex++;
            }

            return jumpIndex;
        }

        private int GetDurationInSamples(int duration)
        {
            double dur = (duration / 1000) * m_rawSampleRate;
            return (int)Math.Round(dur);
        }

        public SignalTestResultHolder RunTests()
        {
            SignalTestResultHolder resultHolder = new SignalTestResultHolder();
            resultHolder.CandidateFileStartIndex = CandidateFileStartIndex;
            resultHolder.CandidateFileStartDT = CandidateFileStartDT;
            resultHolder.CandidateFileNumSamples = m_candidateData.Length;

            for (int i = 0; i < m_taskList.Count; i++)
            {
                SignalTestTask task = m_taskList[i];
                if (task.testFunc != SignalTestFunc.Marker)
                {
                    int taskStartIndex = GetCandidateSampleIndex(task.startTime);
                    //Adjust test start time to skip first second
                    task.startTime = task.startTime.Add(TimeSpan.FromMilliseconds(500));

                    SignalTestResult result = RunTest(task);
                    resultHolder.AddResult(result);

                    //Build the test result table data and add to report array
                    List<string> resultRow = new List<string>();
                    resultRow.Add(StringTools.SpaceCamelCase(task.testFunc.ToString()));

                    foreach (KeyValuePair<string, string> kv in result.GetInfo()) {
                        resultRow.Add(kv.Key + " " + kv.Value);
                    }

                    AddReportData(resultRow.ToArray());
                }
            }

            return resultHolder;
        }

        public SignalTestResult RunTest(SignalTestTask t)
        {
            SignalTestResult res = new SignalTestResult();

            switch (t.testFunc)
            {
                case SignalTestFunc.DynamicRange:

                    if (t.outputLead == OutputLead_E.Lead_RA) {
                        res = DynamicRange(m_candidateData, m_chanResolution, t.startTime, t.durationTS(), t.frequency, t.amplitude);
                    }

                    break;

                case SignalTestFunc.InputImpedance:
                    /* if (task.outputLead == OutputLead_E.Lead_RA)
                        InputImpedance( _referenceData, _candidateData,  _chanResolutionRA, t.startTime, t.durationTS() )
                    */
                    break;

                case SignalTestFunc.GainAccuracy:

                    if (t.outputLead == OutputLead_E.Lead_RA) {
                        res = GainAccuracy(m_candidateData, m_chanResolution, t.startTime, t.durationTS(), t.frequency, t.amplitude);
                    }
                    break;

                case SignalTestFunc.FrequencyResponse:
                    /* if (task.outputLead == OutputLead_E.Lead_RA){
                        FrequencyResponse( _candidateData, _chanResolution, t.startTime, t.durationTS(), t.frequency, t.amplitude )
                    } */
                    break;

                default: break;
            }

            return res;
        }

        private SignalTestResult DynamicRange(short[] candidateData, double chanResolution,
            DateTime startTime, TimeSpan duration, double freq, double refAmpl)
        {
            /** ------------------------------------------------ **/
            /** Test for linearity and dynamic range (RA and V4) **/
            /** ------------------------------------------------ **/
            /* The output signal amplitude referred to input shall not change 
             * by more than 10% or 50 μV, whichever is greater -> PASS */

            SignalTestResult result = new SignalTestResult();
            result.IsPass = false;
            const double PERCENT_LIMIT = 10;
            TimeSpan chunkSize = TimeSpan.FromMilliseconds(1.3 * (1000 / freq));
            DateTime maxTime = startTime.Add(duration).Add(TimeSpan.FromMilliseconds(500)); 
            if (freq <= 0) freq = 1;
            int numberOfPasses = 0;

            int startIndex = GetCandidateSampleIndex(startTime);
            int jumpIndex = FindJumpIndex(candidateData, startIndex, 700);
            if (!(jumpIndex > 0)) jumpIndex = candidateData.Length - 1;

            //Repeat the test until we find stable signal within tolerance
            while (startTime < maxTime && startIndex < jumpIndex)
            {
                SignalChunk sampleChunk = GetCandidateSampleChunk(startTime, chunkSize);
                double pkpkCandidate = PeakToPeak(Array.ConvertAll(sampleChunk.Samples, x => (double)x), chanResolution);
                double absoluteDiff = Math.Abs(refAmpl - pkpkCandidate);
                double percentDiff = -1;

                //Calculate percentage difference between measured amplitude and the reference amplitude
                if (absoluteDiff > 0 && refAmpl > 0) {
                    percentDiff = absoluteDiff / refAmpl * 100;
                }

                //If we find a stable signal which passes the test, save the result information
                if (percentDiff >= 0 && percentDiff <= PERCENT_LIMIT) {
                    numberOfPasses++;
                    if (numberOfPasses >= 2) {
                        result.SampleChunk = sampleChunk;
                        result.AddInfo("index", GetCandidateSampleIndex(startTime).ToString() + " length " + sampleChunk.Samples.Length);
                        result.AddInfo("pkpk", String.Format("{0:0.0000}mV", pkpkCandidate));
                        result.AddInfo("ref", String.Format("{0:0.0000}mV", refAmpl));
                        result.AddInfo("diff", String.Format("{0:0.0000}mV", absoluteDiff));
                        result.AddInfo("diff", String.Format("{0:0.0}%", percentDiff));
                        break;
                    }
                }

                //Check if next chunk will be over the test time window
                if (startTime.Add(chunkSize) >= maxTime || (jumpIndex > 0 && GetCandidateSampleIndex(startTime.Add(chunkSize)) >= jumpIndex)) {
                    if (percentDiff > PERCENT_LIMIT) {
                        //Reached the end of the test time window and the signal does not meet the required tolerance
                        result.SampleChunk = sampleChunk;
                        result.AddInfo("index", GetCandidateSampleIndex(startTime).ToString() + " length " + sampleChunk.Samples.Length);
                        result.AddInfo("pkpk", String.Format("{0:0.0000}mV", pkpkCandidate));
                        result.AddInfo("ref", String.Format("{0:0.0000}mV", refAmpl));
                        result.AddInfo("diff", String.Format("{0:0.0000}mV", absoluteDiff));
                        result.AddInfo("diff", String.Format("{0:0.0}%", percentDiff));
                    }
                    break;
                };

                startTime = startTime.Add(chunkSize);
                startIndex = GetCandidateSampleIndex(startTime);
            }

            result.IsPass = (numberOfPasses >= 2) ? true : false;
            result.AddInfo("outcome", (result.IsPass ? "PASS" : "FAIL"));

            return result;
        }

        public string InputImpedance(double[] referenceData, short[] candidateData,
            double chanResolution, DateTime startTime, TimeSpan duration)
        {
            /** ------------------------------------ **/
            /** Test for input impedance             **/
            /** ------------------------------------ **/
            /* Output Sine wave(5 mV, 10 Hz) and measure Vpp as reference Vref.
             * Uncheck S2(with 620KΩ) and compare Vpp with Vref for Pass Criterion.

             * Set DC Offset to 300 mV and wait for 10s.Compare Vpp with Vref for Pass Criterion.
             * Set DC Offset to - 300 mV and wait for 10s.Compare Vpp with Vref for Pass Criterion.
            
             * Pass criterion: Measure “without 620KΩ impedance” Vpp(lead II / I / III / V1 - V6) as Vref.
             * Compare “with 620KΩ impedance” Vpp > 0.94 Vref->PASS.
             */

            string results = "";
            int numberOfFails = 0;

            return results;
        }

        public SignalTestResult GainAccuracy(short[] candidateData, double chanResolution,
            DateTime startTime, TimeSpan duration, double freq, double refAmpl)
        {
            /** ------------------------------------ **/
            /** Test for gain accuracy               **/
            /** ------------------------------------ **/
            /* The output shall be reproduced with a maximum 
             * amplitude error of +/- 10% to the test signal, referred to input. 
             */
            SignalTestResult result = new SignalTestResult();
            result.IsPass = false;
            const double PERCENT_LIMIT = 10;
            TimeSpan chunkSize = TimeSpan.FromMilliseconds(1.3 * (1000 / freq));
            DateTime maxTime = startTime.Add(duration).Add(TimeSpan.FromMilliseconds(500));
            if (freq <= 0) freq = 1;
            int numberOfPasses = 0;

            int startIndex = GetCandidateSampleIndex(startTime);
            int jumpIndex = FindJumpIndex(candidateData, startIndex, 700);
            if (!(jumpIndex > 0)) jumpIndex = candidateData.Length - 1;

            //Repeat the test until we find stable signal within tolerance
            while (startTime < maxTime && startIndex < jumpIndex)
            {
                SignalChunk sampleChunk = GetCandidateSampleChunk(startTime, chunkSize);

                double pkpkCandidate = PeakToPeak(Array.ConvertAll(sampleChunk.Samples, x => (double)x), chanResolution);
                double absoluteDiff = Math.Abs(refAmpl - pkpkCandidate);
                double percentDiff = -1;

                //Calculate percentage difference between measured amplitude and the reference amplitude
                if (absoluteDiff > 0 && refAmpl > 0) {
                    percentDiff = absoluteDiff / refAmpl * 100;
                }

                //If we find a stable signal which passes the test, save the result information
                if (percentDiff >= 0 && percentDiff <= PERCENT_LIMIT) {
                    numberOfPasses++;
                    if (numberOfPasses >= 2) {
                        result.SampleChunk = sampleChunk;
                        result.AddInfo("index", GetCandidateSampleIndex(startTime).ToString() + " length " + sampleChunk.Samples.Length);
                        result.AddInfo("pkpk", String.Format("{0:0.0000}mV", pkpkCandidate));
                        result.AddInfo("ref", String.Format("{0:0.0000}mV", refAmpl));
                        result.AddInfo("diff", String.Format("{0:0.0000}mV", absoluteDiff));
                        result.AddInfo("diff", String.Format("{0:0.0}%", percentDiff));
                        break;
                    }
                }

                //Check if next chunk will be over the test time window
                if (startTime.Add(chunkSize) >= maxTime || (jumpIndex > 0 && GetCandidateSampleIndex(startTime.Add(chunkSize)) >= jumpIndex))
                {
                    if (percentDiff > PERCENT_LIMIT) {
                        //Reached the end of the test time window and the signal does not meet the required tolerance
                        result.SampleChunk = sampleChunk;
                        result.AddInfo("index", GetCandidateSampleIndex(startTime).ToString() + " length " + sampleChunk.Samples.Length);
                        result.AddInfo("pkpk", String.Format("{0:0.0000}mV", pkpkCandidate));
                        result.AddInfo("ref", String.Format("{0:0.0000}mV", refAmpl));
                        result.AddInfo("diff", String.Format("{0:0.0000}mV", absoluteDiff));
                        result.AddInfo("diff", String.Format("{0:0.0}%", percentDiff));
                    }
                    break;
                };

                startTime = startTime.Add(chunkSize);
                startIndex = GetCandidateSampleIndex(startTime);
            }

            result.IsPass = (numberOfPasses >= 2) ? true : false;
            result.AddInfo("outcome", (result.IsPass ? "PASS" : "FAIL"));

            return result;
        }

        public double PeakToPeak(double[] data)
        {
            double min = 100000;
            double max = -100000;
            double sample;

            for (int i = 0; i < data.Length; i++) {
                sample = data[i];
                if (sample < min) min = sample;
                if (sample > max) max = sample;
            }

            return (max - min);
        }

        public double PeakToPeak(double[] data, double chanResolution)
        {
            double min = 100000;
            double max = -100000;
            double sample;

            for (int i = 0; i < data.Length; i++) {
                sample = data[i];
                if (sample < min) min = sample;
                if (sample > max) max = sample;
            }

            return (max - min) * chanResolution;
        }

        public string FrequencyResponse(short[] candidateData, double chanResolution,
            DateTime startSampleTime, TimeSpan duration, double frequency, double referenceAmplitude)
        {
            /** -------------------------------------------- **/
            /** Test for frequency response                  **/
            /** -------------------------------------------- **/

            string results = "";
            int numberOfFails = 0;



            return results;
        }

        private DateTime GetDateTimeOfSample(int sampleIndex)
        {
            return CandidateFileStartDT.Add(TimeSpan.FromSeconds(sampleIndex / AppConfig.PatchSampleRate));
        }

        public int GetCandidateSampleIndex(DateTime dateTimeOfSample) {
            int sampleIndex = 0;
            TimeSpan timeDiff = dateTimeOfSample.Subtract(CandidateFileStartDT);
            sampleIndex = Convert.ToInt32(Math.Round(timeDiff.TotalSeconds * m_rawSampleRate));
            return sampleIndex;
        }

        private SignalChunk GetCandidateSampleChunk(DateTime startTime, TimeSpan duration) {
            int start = GetCandidateSampleIndex(startTime);
            int length = (int)Math.Round(duration.TotalMilliseconds / (1000 / m_rawSampleRate));
            return new SignalChunk(startTime, start, duration, m_candidateData.Skip(start).Take(length).ToArray());
        }

    }

    public class SignalTestResult
    {
        public bool IsPass { get; set; }
        public SignalChunk SampleChunk { get; set; }

        List<KeyValuePair<string, string>> m_resultData = new List<KeyValuePair<string, string>>();

        public SignalTestResult() { }

        public void AddInfo(string key, string val) {
            m_resultData.Add(new KeyValuePair<string, string>(key, val));
        }

        public List<KeyValuePair<string, string>> GetInfo() {
            return m_resultData;
        }

    }

    public class SignalChunk {

        public short[] Samples { get; private set; }
        public DateTime StartTime { get; private set; }
        public int StartIndex { get; private set; }
        public TimeSpan Duration { get; private set; }

        public SignalChunk(DateTime startTime, int startIndex, TimeSpan duration, short[] samples)
        {
            StartTime = startTime;
            StartIndex = startIndex;
            Duration = duration;
            Samples = samples;
        }
    }

}
