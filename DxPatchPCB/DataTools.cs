﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows;
using System.Xml;

namespace DxPatchPCBTestApp
{
    public struct CMRR_task
    {
        public int step;
        public string testFunc;
        public string outputFunc;
        public string dcOffset;
        public double duration;
    }

    public static class DataTools
    {
        private static XmlNodeList GetXmlNodes(string xmlFilePath, string xPathSelector)
        {
            XmlNodeList nodes;
            XmlDocument xmlDoc = new XmlDocument();

            try{
                xmlDoc.Load(xmlFilePath);
                nodes = xmlDoc.DocumentElement.SelectNodes(xPathSelector);
            } catch (FileNotFoundException){
                throw;
            }

            return nodes;
        }

        public static SignalTask[] LoadSignalTasksFromXml(string sigTasksXmlScript)
        {
            List<SignalTask> taskList = new List<SignalTask>();
            foreach (XmlNode xmlNode in GetXmlNodes(sigTasksXmlScript, "/SignalTasks/SignalTask")){
                taskList.Add(new SignalTask(xmlNode.Attributes));
            }
            return taskList.ToArray();
        }

        public static List<SignalTestTask> LoadSignalTestsFromXml(string tasksXmlScript)
        {
            List<SignalTestTask> taskList = new List<SignalTestTask>();
            foreach (XmlNode xmlNode in GetXmlNodes(tasksXmlScript, "/SignalTestTasks/SignalTestTask")){
                taskList.Add(new SignalTestTask(xmlNode.Attributes));
            }
            return taskList;
        }

        public static string GetHemoFirmwareVersion(string hemoFilePath)
        {
            string firmwareVersion = "";

            //The first 16 bytes of the RAW file header should contain the firmware version
            byte[] byteData = new byte[16];

            try
            {
                using (BinaryReader reader = new BinaryReader(new FileStream(hemoFilePath, FileMode.Open))){
                    reader.Read(byteData, 0, 16);
                    reader.Close();
                }
                firmwareVersion = System.Text.Encoding.UTF8.GetString(byteData);
            }
            catch (Exception ex) {
                MessageBox.Show("Error reading RAW file header," 
                + " could not get firmware version.\n" + ex.Message);
            }
            
            return firmwareVersion;
        }

        public static short[] GetHemoData(string hemoFilePath, int chanIndex, bool adjust)
        {
            List<short> listChanData = new List<short>();
            InoviseCOM.HemoFile hemo = new InoviseCOM.HemoFile();
            hemo.FileName = hemoFilePath;

            if (hemo.Open() == 0) {
                short[] chanData = (short[])hemo.ChannelData(chanIndex, 0, hemo.DurationInSamples);

                for (int i = 0; i < hemo.DurationInSamples; i++){
                    listChanData.Add(adjust ? BitConversion(chanData[i]) : chanData[i]);
                }
                hemo.Close();
            }

            return listChanData.ToArray();
        }

        public static short BitConversion(int sample)
        {
            int adjustedSample;
            const int TWELVE_BIT_MIN = -2048;
            const int TWELVE_BIT_MAX = 2047;

            //Convert 16 bit to 12 bit, and center around x axis
            if (sample < 0) {
                adjustedSample = -1 * (int)Math.Round(
                    ((double)sample / short.MinValue) * Math.Abs(TWELVE_BIT_MIN));
                adjustedSample += Math.Abs(TWELVE_BIT_MIN);
            }
            else {
                adjustedSample = (int)Math.Round(
                    ((double)sample / short.MaxValue) * TWELVE_BIT_MAX);
                adjustedSample -= TWELVE_BIT_MAX;
            }

            return (short)adjustedSample;
        }

        public static System.Drawing.Point[] GetHemoDataAsPoints(
            string hemoFilePath, int chanIndex, bool adjust)
        {
            var imagePoints = new List<System.Drawing.Point>();

            short[] hemoData = GetHemoData(hemoFilePath, chanIndex, adjust);

            if (hemoData != null && hemoData.Length >= 1) {
                for (int i = 0; i < hemoData.Length; i++) {
                    imagePoints.Add(new System.Drawing.Point(i, hemoData[i]));
                }
            }

            return imagePoints.ToArray();
        }

        public static string SaveReferenceDataToCsv(DateTime dateTimeSignalStarted, List<double[]> taList, string csvFile)
        {
            string message = "";
            List<object[]> referenceData = new List<object[]>();

            referenceData = AdjustTimes(dateTimeSignalStarted, taList);

            try{
                using (var sw = new StreamWriter(csvFile)){

                    foreach (object[] timeAmp in referenceData) {
                        DateTime moment = (DateTime)timeAmp[0];
                        double amplitude = (double)timeAmp[1];

                        sw.WriteLine(string.Format("{0},{1}", moment.ToString(AppConfig.DateTimeFormat), amplitude));
                        sw.Flush();
                    }
                    message += "Saved reference data: " + csvFile + "\n";
                    sw.Close();
                }
            } catch (Exception ex){
                message += "\n" + ex.Message;
            }

            return message;
        }

        public static List<object[]> AdjustTimes(DateTime dateTimeStartOfSignal, List<double[]> taList)
        {
            /* SECG timing starts from zero each time signal settings are changed,
            * so need to correct the times to make it a continuous scale */

            List<object[]> adjustedList = new List<object[]>();

            DateTime timeTracker = dateTimeStartOfSignal;
            double prev = 0;
            double next = 0;
            double diff = 0;

            for (int i = 1; i < taList.Count; i++) {
                prev = taList[i - 1][0];
                next = taList[1][0];

                if (prev < next) { diff = next - prev; }
                else if (prev > next) { diff = next; }
                else { diff = 0; }

                timeTracker = timeTracker.Add(TimeSpan.FromSeconds(diff));

                try{
                    adjustedList.Add(new object[] { timeTracker, taList[i][1] });
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            }

            return adjustedList;
        }

        public static string SaveTestScript(Queue<SignalTask> sigTestQueue, string filePath)
        {
            string message = "";

            try {
                using (var sw = new StreamWriter(filePath)) {
                    sw.WriteLine("<SignalTestTasks>");

                    foreach (SignalTask task in sigTestQueue)
                    {
                        if ("" + task.testFunc != ""){
                            string strXml = "<SignalTestTask"
                                + " testFunc=\"" + task.testFunc + "\""
                                + " outputLead=\"" + task.outputLeads[0].ToString() + "\""
                                + " outputFunc=\"" + task.outputFunc.ToString() + "\""
                                + " amplitude=\"" + task.amplitude.ToString() + "\""
                                + " frequency=\"" + task.frequency.ToString() + "\""
                                + " startTime=\"" + task.dateTimeExecuted.ToString(AppConfig.DateTimeFormat) + "\""
                                + " duration=\"" + task.duration.ToString() + "\""
                                + " width=\"" + task.width.ToString() + "\""
                                + " />";
                            sw.WriteLine(strXml);
                        }
                    }

                    sw.WriteLine("</SignalTestTasks>");
                    sw.Close();
                }
                message += "\nSaved test script: " + filePath + "\n";
            }
            catch (Exception ex) { message += ex.Message; }

            return message;
        }
    }
}
