﻿using System;
using System.Windows;
using System.IO;
using System.Windows.Threading;
using System.Reflection;

namespace DxPatchPCBTestApp
{
    public partial class WdwRunSignalTest : Window
    {
        private SignalTester m_signalTester;
        private SignalTestReport m_report;
        private string m_scriptFile;
        private string m_candidateFile;
        private int m_sampleRate;
        private double m_resolutionRA;
        private string m_ppid;
        private bool m_moveFiles = false;

        public WdwRunSignalTest(string scriptFilePath, string candidateFilePath,
            int sampleRate, double resolutionRA, string ppid, bool adjustData = false, bool moveFiles = false)
        {
            InitializeComponent();
  
            m_scriptFile = scriptFilePath;
            m_candidateFile = candidateFilePath;
            m_sampleRate = sampleRate;
            m_resolutionRA = resolutionRA;
            m_ppid = ppid;
            m_moveFiles = moveFiles;

            /* If desired, move RAW and test script files to 
            output directory with name CH_XX_ID_XXXXX */
            if (m_moveFiles) {
                MoveRawFile();
                MoveScriptFile();
            }
        }

        private string DriveLetterToUpper(string path)
        {
            string result = "";
            path = path.Trim();
            if (path.Length >= 1) {
                result += path.Substring(0, 1).ToUpper() + path.Remove(0, 1);
            }
            return result;
        }

        public void MoveRawFile()
        {
            string id = m_ppid.Replace(" ", "_");

            try {
                DirectoryInfo dir = Directory.CreateDirectory(
                    string.Format("{0}\\{1}", AppConfig.OutputDirectory, id));

                string newRawPath = dir.FullName + "\\" + id
                    + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".RAW";

                if (newRawPath.ToUpper() != m_candidateFile.ToUpper()) {
                    File.Copy(m_candidateFile, newRawPath, true);
                    if (Path.GetDirectoryName(m_candidateFile).ToUpper()
                        != Path.GetDirectoryName(newRawPath).ToUpper()) {
                        File.Delete(m_candidateFile);
                    };
                    m_candidateFile = newRawPath;
                }
            }
            catch (Exception ex) {
                Console.WriteLine("Error moving file during test.\n" + ex.Message);
            }
        }

        private void MoveScriptFile()
        {
            try
            {
                string newScriptPath = Path.GetDirectoryName(m_candidateFile) +
                    "\\" + Path.GetFileName(m_scriptFile);

                if (newScriptPath.ToUpper() != m_scriptFile.ToUpper()) {
                    File.Copy(m_scriptFile, newScriptPath, true);
                    m_scriptFile = newScriptPath;
                }
            }
            catch (Exception ex) {
                Console.WriteLine("Error moving file during test.\n" + ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TxtReportStatus.Text = "Running tests.  This may take several seconds to complete...";

            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>{

                m_signalTester = new SignalTester(
                    m_scriptFile,
                    m_candidateFile,
                    m_sampleRate,
                    m_resolutionRA
                );

                /* ******************************************************** */
                /* Run signal tests */
                /* ******************************************************** */
                SignalTestResultHolder resultObject = m_signalTester.RunTests();

                var signalPoints = DataTools.GetHemoDataAsPoints(m_candidateFile, 1, true);
  
                m_report = new SignalTestReport(
                    resultObject,
                    DateTime.Now,
                    m_ppid,
                    DataTools.GetHemoFirmwareVersion(m_candidateFile),
                    m_signalTester.ReportData,
                    signalPoints,
                    m_candidateFile,
                    m_scriptFile
                );

                string reportFilePath = SaveReport(m_report, Path.GetDirectoryName(m_candidateFile), m_ppid, resultObject.IsPass());
                TxtReportStatus.Text = "Test complete: " + reportFilePath;
                WebBrowserTestResults.Navigate(@"file:///" + reportFilePath);
                ChangeRawFileName(m_candidateFile, resultObject.IsPass());
            }));

        }

        private string SaveReport(SignalTestReport report, string dirPath, string ppid, bool isPass)
        {
            string reportPath = "";

            try
            {
                reportPath = dirPath
                 + @"\Report_" + ppid.Replace(" ", "_") + "_"
                 + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "_"
                 + (isPass ? "PASS" : "FAIL") + ".pdf";

                m_report.SavePDF(reportPath);
            }
            catch (Exception ex) { reportPath = "Error saving report file. " + ex.Message;}
            
            return reportPath;
        }

        private void ChangeRawFileName(string fileName, bool isPass)
        {
            string newFileName = "";
            newFileName = RemoveLast(fileName, "PASS");
            newFileName = RemoveLast(fileName, "FAIL");
            newFileName = RemoveLast(fileName, ".RAW");
            newFileName = newFileName + (isPass ? "_PASS" : "_FAIL") + ".RAW";

            try { File.Move(fileName, newFileName); }
            catch (Exception ex) { MessageBox.Show("Error moving RAW file.\n" + ex.Message); }
        }

        private string RemoveLast(string input, string substring)
        {
            string result = "" + input;
            int i = input.LastIndexOf(substring);
            if (i >= 0) result = input.Remove(i, substring.Length);
            return result;
        }
    }
}
