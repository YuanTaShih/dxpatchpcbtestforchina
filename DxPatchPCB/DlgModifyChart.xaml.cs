﻿using System;
using System.Windows;

namespace DxPatchPCBTestApp
{
    /// <summary>
    /// Interaction logic for DialogModifyChartRange.xaml
    /// </summary>
    public partial class DialogModifyChart : Window
    {
        private const string CHART_RANGE_PLACEHOLDER = "xxx:xxx";
        private Chart _chart;

        public DialogModifyChart(Chart chart)
        {
            InitializeComponent();
            _chart = chart;
            TxtChartRange.Text = _chart.m_dataRangeStart + ":" + _chart.m_dataRangeEnd;
        }

        private void TxtChartRange_GetFocus(object sender, RoutedEventArgs e)
        {
            if (TxtChartRange.Text == CHART_RANGE_PLACEHOLDER)
                TxtChartRange.Text = string.Empty;
        }

        private void TxtChartRange_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TxtChartRange.Text == string.Empty)
                TxtChartRange.Text = CHART_RANGE_PLACEHOLDER;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
    }
}
