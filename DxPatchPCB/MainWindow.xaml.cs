﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Documents;
using System.Windows.Threading;
using System.Windows.Controls;
using System.IO;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Timers;
using System.Threading;
using Microsoft.Win32;
using WhaleTeqSECG_SDK;
using System.Reflection;
using System.Diagnostics;
using System.Xml;



namespace DxPatchPCBTestApp
{
    public partial class MainWindow : Window
    {
        private System.Timers.Timer m_driveCheckTimer = new System.Timers.Timer();

        private delegate void TimeAmplitudeCBDelegate(double time, double amp);
        private TimeAmplitudeCBDelegate m_delegateTimeAmpFunc;
        private DateTime m_dateTimeSignalStarted;
        private List<double[]> m_timeAmpList = new List<double[]>();

        private DispatcherTimer m_signalTaskTimer = new DispatcherTimer();
        private SignalTask[] m_sigGenTasks;
        private Queue<SignalTask> m_sigGenQueue = new Queue<SignalTask>();
        private Queue<SignalTask> m_sigTestQueue = new Queue<SignalTask>();
        private double m_countDown = 0;
        private double m_taskDuration = 0;
        private bool m_taskIsRunning = false;
        private DateTime m_taskStartTime;

        private const string SIG_TEST_SCRIPT_FILE = "sig-test-script.xml";
        private const string REFERENCE_DATA_FILE = "reference_data.csv";
        private const double DRIVE_CHECK_INTERVAL = 900;

        private System.Timers.Timer timer1 = new System.Timers.Timer();
        CMRR_task[] cmrr_task;
        int cmrr_test_step;




        public MainWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Title += " v" + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion + " For China";

            AppConfig.LoadConfig();
            TxtSignalGenScriptFile.Text = AppConfig.SignalGeneratorScriptPath;
            TxtReportDir.Text = AppConfig.OutputDirectory;
            TxtConfigFilePath.Text = "File: " + AppConfig.GetConfigFilePath();
            TextboxConsole.Document.Blocks.Clear();

            //Check if the InoviseCom server object is registered and available
            try
            {
                InoviseCOM.HemoFile hemo = new InoviseCOM.HemoFile();
            }catch (Exception ex) {
                string errorMmessage = "Could not instantiate InoviseCOM object, "
                    + "check the InoviseCOM server is registered.\n" + ex.Message + ".";
                WriteConsole(errorMmessage, color: 2);
                MessageBox.Show(errorMmessage);
            }

            //To record reference signal data from the SECG device
            m_delegateTimeAmpFunc = new TimeAmplitudeCBDelegate(TimeAmplitudeCallBack);
            IntPtr intpPtrTimeAmpFunc = Marshal.GetFunctionPointerForDelegate(m_delegateTimeAmpFunc);

            bool secgOk = false;

            try {
                secgOk = SECG_SDK.InitSECG();
                SECG_SDK.RegisterTimeAmpCB(intpPtrTimeAmpFunc);
            } catch (DllNotFoundException ex) {
                string errorMessage = "Error: could not find WhaleTeqSECG_SDK.dll.\n" + ex.Message;
                MessageBox.Show(errorMessage);
                Application.Current.Shutdown();
            }

            if (secgOk){ WriteConsole("SECG device initialized successfully.", color: 1); }
            else { WriteConsole("SECG device failed to initialize.", color: 2); }

            SECG_SDK.CloseSECG();

            m_driveCheckTimer.Elapsed += new ElapsedEventHandler(DriveCheckEvent);
            m_driveCheckTimer.Interval = DRIVE_CHECK_INTERVAL;  
            m_driveCheckTimer.Start();

            m_signalTaskTimer.Tick += SignalTaskTimer_Tick;
            m_signalTaskTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);

            LoadSignalGenTasks();
        }

        private void TimeAmplitudeCallBack(double time, double amp){
            m_timeAmpList.Add(new double[] { time, amp });
        }

        public void DriveCheckEvent(object source, ElapsedEventArgs e)
        {
            //Use RAW file from USB drives
            if (DriveHelper.DrivesChanged())
            {
                string[] rawFilePaths = DriveHelper.GetRawFilesFromDrives();
                if (rawFilePaths == null || rawFilePaths.Length < 1) return;

                //Check windows not open already
                Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
                {
                    foreach (Window w in Application.Current.Windows)
                    {
                        if (w is DlgGetUsbRawFile || w is WdwRunSignalTest) return;
                    }


                    for( int i=0;i< rawFilePaths.Length; i++)
                    {
                        bool CMRR = true;

                        string rawFilePath = rawFilePaths[i];
                        if (rawFilePath.Substring(rawFilePath.Length - 3, 3).Equals("RAW"))
                            CMRR = false;
                        else if (rawFilePath.Substring(rawFilePath.Length - 3, 3).Equals("001"))
                            CMRR = true;
                        else
                            CMRR = false;

                        DlgGetUsbRawFile dlgGetFile = new DlgGetUsbRawFile(rawFilePath);

                        //Check if the user wants to go ahead and run the test on this raw file
                        if (dlgGetFile.ShowDialog() == true)
                        {

                            ShowSignalTestWindow(
                                AppConfig.OutputDirectory + "\\" + SIG_TEST_SCRIPT_FILE,
                                rawFilePath,
                                AppConfig.PatchSampleRate,
                                AppConfig.PatchEcgResolution,
                                dlgGetFile.TxtPPID.Text.Trim(),
                                adjustData: true,
                                moveFiles: true,
                                CMRR: CMRR
                            );
                        }
                    }
                }
                ));
            }
        }

        private void ShowSignalTestWindow(string scriptFilePath, string rawFilePath, int sampleRate, double ecgResolution, string ppid, bool adjustData, bool moveFiles, bool CMRR)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() => {

                foreach (string path in new string[] { scriptFilePath, rawFilePath }){
                    if (!File.Exists(path)){
                        MessageBox.Show("File not found.\n" + path);
                        return;
                    }
                }

                string id = ppid.Replace(" ", "_");
                try
                {
                   
                    DirectoryInfo dir = Directory.CreateDirectory(
                        string.Format("{0}\\{1}", AppConfig.OutputDirectory, id));

                    string newRawPath = "";
                    if (CMRR)
                        newRawPath = dir.FullName + "\\" + id+ "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss")+"_NOISE" + ".RAW";
                    else
                        newRawPath = dir.FullName + "\\" + id + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".RAW";

                    if (newRawPath.ToUpper() != rawFilePath.ToUpper())
                    {
                        File.Copy(rawFilePath, newRawPath, true);
                        if (Path.GetDirectoryName(rawFilePath).ToUpper()
                            != Path.GetDirectoryName(newRawPath).ToUpper())
                        {
                            File.Delete(rawFilePath);
                        };
                        rawFilePath = newRawPath;

                        BackgroundWorker csvExportWorker = new BackgroundWorker();
                        csvExportWorker.WorkerReportsProgress = true;
                        csvExportWorker.DoWork += csvExportWorker_DoWork;
                        csvExportWorker.ProgressChanged += csvExportWorker_ProgressChanged;
                        csvExportWorker.RunWorkerAsync(new object[] { rawFilePath, true });

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error moving file during test.\n" + ex.Message);
                }

                try
                {
                    string newScriptPath = Path.GetDirectoryName(rawFilePath) +
                        "\\" + Path.GetFileName(scriptFilePath);

                    if (newScriptPath.ToUpper() != scriptFilePath.ToUpper())
                    {
                        File.Copy(scriptFilePath, newScriptPath, true);
                        scriptFilePath = newScriptPath;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error moving file during test.\n" + ex.Message);
                }

                //try
                //{
                //    WdwRunSignalTest testWindow = new WdwRunSignalTest(
                //        scriptFilePath,
                //        rawFilePath,
                //        sampleRate,
                //        ecgResolution,
                //        ppid,
                //        adjustData: adjustData,
                //        moveFiles: moveFiles
                //    );
                //    testWindow.Show();

                //}
                //catch (FileNotFoundException ex)
                //{
                //    MessageBox.Show("File not found. " + ex.Message);
                //}
            }));
        }

        private void LoadSignalGenTasks()
        {
            if (File.Exists(TxtSignalGenScriptFile.Text)) {
                m_sigGenTasks = DataTools.LoadSignalTasksFromXml(TxtSignalGenScriptFile.Text);

                //Fill the datagrid with the task data
                List<object> taskDataRows = new List<object>();
                foreach (SignalTask task in m_sigGenTasks)
                    taskDataRows.Add(task.toDataRow());

                dataGridSignalTasks.ItemsSource = taskDataRows;
                dataGridSignalTasks.SelectedItem = dataGridSignalTasks.Items.GetItemAt(0);
            }
            else{ MessageBox.Show("File not found: " + TxtSignalGenScriptFile.Text); }
        }

        private void GetFilePathFromDialogue(TextBox textboxFilePath)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                textboxFilePath.Text = openFileDialog.FileName;
        }

        private void BtnSignalScriptFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true) {
                TxtSignalGenScriptFile.Text = openFileDialog.FileName;
                LoadSignalGenTasks();
            }
        }

        private void TextboxConsole_TextChanged(object sender, TextChangedEventArgs e) {
            TextboxConsole.ScrollToEnd();
        }

        private void WriteConsole(string text, bool isBold = false, int color = 0)
        {
            SolidColorBrush[] brushes = new SolidColorBrush[] {
                Brushes.Black, Brushes.Green, Brushes.Red
            };

            if (color < 0 || color > brushes.Length - 1) color = 0;

            Paragraph textParagraph = new Paragraph();
            textParagraph.Margin = new Thickness(0);
            textParagraph.Inlines.Add(new Run {
                Text = DateTime.Now.ToString(AppConfig.DateTimeFormat) + " " + text,
                FontWeight = isBold ? FontWeights.Bold : FontWeights.Normal,
                Foreground = brushes[color]
            });

            TextboxConsole.Document.Blocks.Add(textParagraph);
        }

        private void WriteConsole_str(string text, bool isBold = false, int color = 0)
        {
            SolidColorBrush[] brushes = new SolidColorBrush[] {
                Brushes.Black, Brushes.Green, Brushes.Red
            };

            if (color < 0 || color > brushes.Length - 1) color = 0;

            Paragraph textParagraph = new Paragraph();
            textParagraph.Margin = new Thickness(0);
            textParagraph.Inlines.Add(new Run
            {
                Text =  text,
                FontWeight = isBold ? FontWeights.Bold : FontWeights.Normal,
                Foreground = brushes[color]
            });

            TextboxConsole.Document.Blocks.Add(textParagraph);
        }

        private bool secgCheck()
        {
            //To record reference signal data from the SECG device
            m_delegateTimeAmpFunc = new TimeAmplitudeCBDelegate(TimeAmplitudeCallBack);
            IntPtr intpPtrTimeAmpFunc = Marshal.GetFunctionPointerForDelegate(m_delegateTimeAmpFunc);
            bool secgOk = false;
            try
            {
                secgOk = SECG_SDK.InitSECG();
                SECG_SDK.RegisterTimeAmpCB(intpPtrTimeAmpFunc);

            }
            catch (DllNotFoundException ex)
            {
                string errorMessage = "Error: could not find WhaleTeqSECG_SDK.dll.\n" + ex.Message;
                MessageBox.Show(errorMessage);
                Application.Current.Shutdown();
                return false;
            }

            if (secgOk)
            {
                ResetSECG();
                //WriteConsole("SECG device initialized successfully.", color: 1);
                System.Windows.Forms.DialogResult myresult = System.Windows.Forms.MessageBox.Show("SECG已确定连接，请启动8ZP7记录器，确认指示灯成功闪烁", "", System.Windows.Forms.MessageBoxButtons.YesNo);
                if (myresult == System.Windows.Forms.DialogResult.No)
                {
                    SECG_SDK.CloseSECG();
                    return false;
                }
                return true;
            }
            else
            {
                //WriteConsole("SECG device failed to initialize.", color: 2);
                System.Windows.Forms.MessageBox.Show("SECG连接失败，请重新连结，重启程式，并确认指示灯没有闪烁", "");
                SECG_SDK.CloseSECG();
                return false;
            }
        }

        private void BtnStartSignal_Click(object sender, RoutedEventArgs e)
        {


            if (secgCheck() == false)
                return;

            if (m_sigGenTasks != null && m_sigGenTasks.Length >= 1 && !m_signalTaskTimer.IsEnabled) {

                m_sigGenQueue.Clear();
                m_sigTestQueue.Clear();

                TxtSignalGenScriptFile.Text = @"c:\temp\sig-gen-script-Input1.xml";

                if (!File.Exists(@"c:\temp\sig-gen-script-Input1.xml"))
                {
                    WriteConsole_str("找不到 c:\\temp\\sig-gen-script-Input1.xml 檔案!");
                    return;
                }
                LoadSignalGenTasks();
                ResetSECG();


                SignalTask startMarkerTask1 = new SignalTask {
                    testFunc = SignalTestFunc.Marker,
                    outputLeads = new List<OutputLead_E> { OutputLead_E.Lead_LA },
                    outputFunc = OutputFunction.Output_Sine,
                    dcOffset = -300,
                    amplitude = 1,
                    frequency = 1,
                    duration = 1000//dcOffset = 1, amplitude = 5, frequency = 0.25, duration = 1000
                };

                SignalTask startMarkerTask2 = new SignalTask {
                    testFunc = SignalTestFunc.Marker,
                    outputLeads = new List<OutputLead_E> { OutputLead_E.Lead_LA },
                    outputFunc = OutputFunction.Output_Square,
                    dcOffset = 300,
                    amplitude = 1,
                    frequency = 1,
                    duration = 20000//dcOffset = 0, amplitude = 1, frequency = 0.25, duration = 16000
                };

                SetSECG(); // Set all Channel On
                m_sigGenQueue.Enqueue(startMarkerTask1);
                m_sigGenQueue.Enqueue(startMarkerTask2);

                //Load the signal tasks into the queue
                foreach (SignalTask task in m_sigGenTasks){
                    m_sigGenQueue.Enqueue(task);
                }

                //Start the timer to process the queue
                m_countDown = 100;
                m_signalTaskTimer.Start();
                m_dateTimeSignalStarted = DateTime.Now;

                TextboxConsole.Document.Blocks.Clear();
                WriteConsole("started tasks.");
            }
        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            cmrr_test_step = 0;

            m_countDown = 0;
            m_signalTaskTimer.Stop();
            dataGridSignalTasks.SelectedItem = dataGridSignalTasks.Items.GetItemAt(0);
            TextboxCountdown.Text = "0.00";
            WriteConsole("Stopped.", isBold: true);
            SaveTestData();
            ResetSECG();
            SECG_SDK.CloseSECG();
        }

        private void SaveTestData()
        {
            WriteConsole(
                DataTools.SaveTestScript(m_sigTestQueue,
                    AppConfig.OutputDirectory + "\\" + SIG_TEST_SCRIPT_FILE),
                isBold: true
            );

            WriteConsole(
                DataTools.SaveReferenceDataToCsv(m_dateTimeSignalStarted, m_timeAmpList,
                    AppConfig.OutputDirectory + "\\" + REFERENCE_DATA_FILE),
                isBold: true
            );
        }

        private void BtnScriptFile_Click(object sender, RoutedEventArgs e) {
            GetFilePathFromDialogue(TxtScriptFile);
        }

        private void SignalTaskTimer_Tick(object sender, EventArgs e)
        {
            if (m_taskIsRunning)
            {
                TimeSpan taskTimeElapsed = DateTime.Now.Subtract(m_taskStartTime);

                if (taskTimeElapsed.TotalMilliseconds >= m_taskDuration){
                    //Task finished
                    m_taskIsRunning = false;
                    m_countDown = 0;
                } else {
                    //Task still running
                    m_countDown = m_taskDuration - taskTimeElapsed.TotalMilliseconds;
                }
           
            } else if(!m_taskIsRunning && m_sigGenQueue.Count <= 0){

                WriteConsole("finished signal tasks.", isBold:true);
                m_countDown = 0;
                m_signalTaskTimer.Stop();
                SaveTestData();
                SECG_SDK.CloseSECG();

            } else if (!m_taskIsRunning && m_sigGenQueue.Count >= 1){
                
                //Start new task
                SignalTask task = m_sigGenQueue.Dequeue();
                SetSignal(task);

                WriteConsole("started signal task.");

                task.dateTimeExecuted = DateTime.Now;
                m_taskStartTime = DateTime.Now;
                m_taskDuration = task.duration;
                m_countDown = m_taskDuration;
                m_taskIsRunning = true;
                m_sigTestQueue.Enqueue(task);

                dataGridSignalTasks.Dispatcher.Invoke((Action)delegate (){
                    //Update UI
                    dataGridSignalTasks.UnselectAll();
                    int nextItemIndex = dataGridSignalTasks.Items.Count - m_sigGenQueue.Count - 1;
                    if (nextItemIndex < 0) nextItemIndex = 0;
                    dataGridSignalTasks.SelectedItem = dataGridSignalTasks.Items.GetItemAt(nextItemIndex);
                });
            }

            TextboxCountdown.Text = string.Format("{0:0.0}", (m_countDown / 1000));
        }

        private void SetSignal(SignalTask task){

            //ResetSECG();

            if (task.outputLeads != null && task.outputLeads.Count >= 1){
                //SECG_SDK.SetOutputLead(task.outputLeads[0], true);
                SECG_SDK.SetDCOffset(task.dcOffset);          
                SECG_SDK.SetAmplitude(task.amplitude);
                SECG_SDK.SetOutputFunc(task.outputFunc);
                SECG_SDK.SetPulseWidth(task.width);
                SECG_SDK.SetFrequency(task.frequency);
            }
        }

        static public void ResetSECG() {

                SECG_SDK.SetOutputFunc(OutputFunction.Output_Off);
                SECG_SDK.SetDCOffset(0);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_RA, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_LA, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_LL, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_V1, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_V2, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_V3, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_V4, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_V5, false);
                SECG_SDK.SetOutputLead(OutputLead_E.Lead_V6, false);

        }
        static public void SetSECG()
        {

            SECG_SDK.SetOutputLead(OutputLead_E.Lead_RA, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_LA, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_LL, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_V1, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_V2, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_V3, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_V4, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_V5, true);
            SECG_SDK.SetOutputLead(OutputLead_E.Lead_V6, true);

        }

        private void btnCandidateFile_Click(object sender, RoutedEventArgs e){
            GetFilePathFromDialogue(TxtCandidateFile);
        }

        private void TxtPPID_GetFocus(object sender, RoutedEventArgs e){
            if (TxtPPID.Text == AppConfig.PPIDPlaceholder)
                TxtPPID.Text = string.Empty;
        }

        private void TxtPPID_LostFocus(object sender, RoutedEventArgs e){
            if (TxtPPID.Text == string.Empty)
                TxtPPID.Text = AppConfig.PPIDPlaceholder;
        }

        private void BtnRunTest_Click(object sender, RoutedEventArgs e)
        {
            ShowSignalTestWindow(
                TxtScriptFile.Text,
                TxtCandidateFile.Text,
                AppConfig.PatchSampleRate,
                AppConfig.PatchEcgResolution,
                TxtPPID.Text,
                adjustData: true,
                moveFiles: false,
                CMRR: false
            );
        }

        /* Tools: Export to CSV */
        private void BtnChoseInputFile_Click(object sender, RoutedEventArgs e){
            GetFilePathFromDialogue(txtRawFilePathExport);
        }

        /* Tools: Export to CSV */
        private void BtnRunExport_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(txtRawFilePathExport.Text)) {
                MessageBox.Show("Data file not found.\n" + txtRawFilePathExport.Text);
                return;
            }

            bool adjust = false;
            if (checkboxAdjustDataExport.IsChecked == true) adjust = true;

            BackgroundWorker csvExportWorker = new BackgroundWorker();
            csvExportWorker.WorkerReportsProgress = true;
            csvExportWorker.DoWork += csvExportWorker_DoWork;
            csvExportWorker.ProgressChanged += csvExportWorker_ProgressChanged;
            csvExportWorker.RunWorkerAsync(new object[] { txtRawFilePathExport.Text, adjust });
        }

        void csvExportWorker_ProgressChanged(object sender, ProgressChangedEventArgs e){
            progressExportCsv.Value = e.ProgressPercentage;
        }

        /* Tools: Export to CSV */
        private void csvExportWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Open the RAW file and export to CSV format
            object[] args = e.Argument as object[];
            InoviseCOM.HemoFile hemo = new InoviseCOM.HemoFile();
            List<short[]> allChannels = new List<short[]>();
            hemo.FileName = (string)args[0];
            bool adjustData = (bool)args[1];
            BackgroundWorker bgWorker = sender as BackgroundWorker;

            if (hemo.Open() == 0)
            {
                int numOfSamples = hemo.DurationInSamples;
                string csvFile = hemo.FileName + ".csv";
                List<string> channelNames = new List<string>();

                //channel 0 --> Heart Sound
                //channel 1 --> V4RA

                bgWorker.ReportProgress(1);

                for (int chanIndex = 0; chanIndex < hemo.NumberOfChannels; chanIndex++){
                    channelNames.Add(hemo.ChannelName[chanIndex]);
                    allChannels.Add((short[])hemo.ChannelData(chanIndex, 0, numOfSamples));
                }

                using (var sw = new StreamWriter(csvFile))
                {
                    sw.WriteLine(string.Join(",", channelNames));

                    for (int sampleIndex = 0; sampleIndex < numOfSamples; sampleIndex++) {
                        List<string> lineItems = new List<string>();

                        for (int chanIndex = 0; chanIndex < hemo.NumberOfChannels; chanIndex++) {
                            int sample = allChannels[chanIndex][sampleIndex];
                            lineItems.Add(
                                (chanIndex <= 1 && adjustData) 
                                ? DataTools.BitConversion(sample).ToString() 
                                : sample.ToString()
                            );
                        }

                        sw.WriteLine(string.Join(",", lineItems));
                        sw.Flush();

                        double percentProgress = Math.Round(100 * (double)sampleIndex / numOfSamples);
                        if (percentProgress >= 2 && percentProgress % 2 == 0){
                            bgWorker.ReportProgress((int)percentProgress);
                        }
                    }
                    sw.Close();
                }



                System.Windows.Forms.DialogResult myresult = System.Windows.Forms.MessageBox.Show("Data exported to file: " + csvFile + "完成! 是否要启动分析程序?", "", System.Windows.Forms.MessageBoxButtons.YesNo);
                if (myresult == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        Process PCBtest = new Process();
                        PCBtest.StartInfo.FileName = @"C:\temp\PCBTest.exe";
                        PCBtest.Start();
                    }
                    catch (Exception )
                    {
                        MessageBox.Show("找不到" + @"c:\temp\PCBTest.exe !");
                    }
                }
                if (myresult == System.Windows.Forms.DialogResult.No)
                {
                    System.Windows.Forms.DialogResult myresult_24 = System.Windows.Forms.MessageBox.Show("是否要启动24H分析程序?", "", System.Windows.Forms.MessageBoxButtons.YesNo);
                    if (myresult_24 == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            Process PCBtest = new Process();
                            PCBtest.StartInfo.FileName = @"C:\temp\PCBTest_24H.exe";
                            PCBtest.Start();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("找不到" + @"c:\temp\PCBTest_24H.exe !");
                        }
                    }
                }

                //MessageBox.Show("Data exported to file: " + csvFile);
                bgWorker.ReportProgress(0);
                hemo.Close();
            }
        }

        private void BtnChoseChartInputFile_Click(object sender, RoutedEventArgs e){
            GetFilePathFromDialogue(TxtChartRawFile);
        }

        private void BtnDrawChart_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(TxtChartRawFile.Text)){
                MessageBox.Show("Data file not found.\n" + TxtChartRawFile.Text);
                return;
            }

            bool adjustData = false;
            if (checkboxAdjustChartData.IsChecked == true) adjustData = true;

            WdwChartViewer chart = new WdwChartViewer(TxtChartRawFile.Text, 1, adjustData);
            chart.Show();  
        }

        private void TxtReportDir_TextChanged(object sender, TextChangedEventArgs e) {
            AppConfig.OutputDirectory = TxtReportDir.Text;
        }

        private void TxtSignalGenScriptFile_TextChanged(object sender, TextChangedEventArgs e) {
            AppConfig.SignalGeneratorScriptPath = TxtSignalGenScriptFile.Text;
        }

        private void Window_Closed(object sender, EventArgs e) {
            AppConfig.OutputDirectory = TxtReportDir.Text;
            AppConfig.SignalGeneratorScriptPath = TxtSignalGenScriptFile.Text;
            AppConfig.SaveConfig();
        }

        private void BtnViewAppConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string configText = File.ReadAllText(AppConfig.GetConfigFilePath());
                MessageBox.Show(configText, "App config text");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error opening config file.\n" + ex.Message);
            }
        }

        private void BtnStartCMRR_Click(object sender, RoutedEventArgs e)
        {
            m_sigGenQueue.Clear();
            m_sigTestQueue.Clear();

            timer1 = new System.Timers.Timer();
            timer1.Interval = 1000;
            timer1.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            string CMRR_xmltask = @"c:\temp\sig-gen-script-Input1_cmrr.xml";

            try
            {
                //Start the timer to process the queue
                TextboxConsole.Document.Blocks.Clear();
                TextboxCountdown.Text = "5";
                WriteConsole_str("Read tasks from: " + CMRR_xmltask);

                cmrr_test_step = 0;
                ReadCMRR_task(CMRR_xmltask);

                WriteConsole_str("Read tasks from: " + CMRR_xmltask + " 成功");
                timer1.Start();
            }
            catch (Exception )
            {
                WriteConsole("Read tasks from: " + CMRR_xmltask + " 失敗");
            }

        }

        private void BtnStart24h_Click(object sender, RoutedEventArgs e)
        {

            m_sigGenQueue.Clear();
            m_sigTestQueue.Clear();

            if (secgCheck() == false)
                return;

            if (m_sigGenTasks != null && m_sigGenTasks.Length >= 1 && !m_signalTaskTimer.IsEnabled)
            {

                m_sigGenQueue.Clear();
                m_sigTestQueue.Clear();

                TxtSignalGenScriptFile.Text = @"c:\temp\sig-gen-script-Input1_24h.xml";
                if (!File.Exists(@"c:\temp\sig-gen-script-Input1_24h.xml"))
                {
                    WriteConsole_str("找不到 c:\\temp\\sig-gen-script-Input1_24h.xml 檔案!");
                    return;
                }    
                LoadSignalGenTasks();

                ResetSECG();

                //Load the signal tasks into the queue
                foreach (SignalTask task in m_sigGenTasks)
                {
                    m_sigGenQueue.Enqueue(task);
                }

                SetSECG();
                //Start the timer to process the queue
                m_countDown = 100;
                m_signalTaskTimer.Start();
                m_dateTimeSignalStarted = DateTime.Now;

                TextboxConsole.Document.Blocks.Clear();
                WriteConsole("started tasks.");
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Action methodDelegate = delegate ()
            {

                TextboxCountdown.Text = (Convert.ToDouble(TextboxCountdown.Text) - 1).ToString();
                if (Convert.ToDouble(TextboxCountdown.Text) == 0)
                {
                    timer1.Stop();
                    CMRRTESTPROCESS();
                    cmrr_test_step++;
                }
            };
            this.Dispatcher.BeginInvoke(methodDelegate);

        }
        private void ReadCMRR_task(string task)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true; // 不處理註解
            settings.IgnoreWhitespace = true; // 跳過空白
            settings.ValidationType = ValidationType.None; // 不驗證任何資料
            XmlReader reader = XmlTextReader.Create(task, settings);
            int count = 0;

            try
            {
                ///////////////////////////////////////////////////////////////////////////////
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            string LocalName = reader.LocalName; // 取得標籤名稱
                                                                 // Step 3: 讀取 FileInfo 標籤的屬性
                            if (LocalName.Equals("SignalTask"))
                                count++;
                            string testFunc = reader["testFunc"];
                            break;
                    }
                }
                reader.Close();
                ///////////////////////////////////////////////////////////////////////////////


                reader = XmlTextReader.Create(task, settings);
                cmrr_task = new CMRR_task[count];
                count = 0;
                // 進入讀取主要部分
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            string LocalName = reader.LocalName; // 取得標籤名稱

                            // Step 3: 讀取 FileInfo 標籤的屬性
                            if (LocalName.Equals("SignalTask"))
                            {
                                cmrr_task[count].step = count;
                                cmrr_task[count].testFunc = reader["testFunc"];
                                cmrr_task[count].outputFunc = reader["outputFunc"];
                                cmrr_task[count].dcOffset = reader["dcOffset"];
                                cmrr_task[count].duration = Convert.ToDouble(reader["duration"]);
                                WriteConsole_str((cmrr_task[count].step + 1).ToString() + ". " + cmrr_task[count].testFunc + ", " + cmrr_task[count].outputFunc + ", DCoffset(mV)" + cmrr_task[count].dcOffset + ", Duration:" + cmrr_task[count].duration.ToString() + "ms.");
                                count++;
                            }
                            break;
                    }
                }
                reader.Close();
            }
            catch(Exception)
            {
                WriteConsole(task + "资料有误!!");
                return;
            }
        }



        private void CMRRTESTPROCESS()
        {
            if (cmrr_test_step < cmrr_task.Length )
            {
                TextboxCountdown.Text = (cmrr_task[cmrr_test_step].duration / 1000).ToString();

                WriteConsole(" 请确认已经将讯号源转至CMRR!\n Step" + (cmrr_task[cmrr_test_step].step + 1).ToString() + ": 测试项目为 " + cmrr_task[cmrr_test_step].outputFunc +
                    "\n 请确认 Offset 为 " + cmrr_task[cmrr_test_step].dcOffset + "\n 请确认 " + cmrr_task[cmrr_test_step].testFunc + "接口正确 !!\n ");

                System.Windows.Forms.DialogResult myresult = System.Windows.Forms.MessageBox.Show(" 请确认已经将讯号源转至CMRR!\n Step" + (cmrr_task[cmrr_test_step].step + 1).ToString() + ": 测试项目为 " + cmrr_task[cmrr_test_step].outputFunc+
                    "\n 请确认 Offset 为 " + cmrr_task[cmrr_test_step].dcOffset + "\n 请确认 " + cmrr_task[cmrr_test_step].testFunc + "接口正确 !!\n \"是(Y)\":开始测试, \"否(N)\":取消测试", "CMRR Test", System.Windows.Forms.MessageBoxButtons.YesNo);

                if (myresult == System.Windows.Forms.DialogResult.Yes)
                    timer1.Start();
                else
                {
                    timer1.Stop();
                    cmrr_test_step = 0;
                    WriteConsole("Noise Test Stopped!");
                }
            }
            else
            {
                timer1.Stop();
                cmrr_test_step = 0;
                WriteConsole("Noise Test compeleted!");
            }


        }
    }
}
