﻿using System;
using System.IO;
using System.Reflection;

namespace DxPatchPCBTestApp
{
    public class AppLog
    {
        public static string LogFileDirPath { get; set; } = @"C:\temp\logs";
        private static string LogName { get; set; } = @"DxPatchPCBTestApp_log_";

        private static string GetDateTimeString()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        public static void Write(string info)
        {
            string logFileNameToday = LogFileDirPath + "\\" + LogName + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            WriteToFile(logFileNameToday, info);
        }

        public static void WriteInExeDir(string info)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            string logFileNameToday = Path.GetDirectoryName(asm.Location) + "\\" + LogName + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            WriteToFile(logFileNameToday, info);
        }

        private static void WriteToFile(string logFileName, string logInfo)
        {
            string dirPath = Path.GetDirectoryName(logFileName);
            logInfo = GetDateTimeString() + " " + logInfo;

            try
            {
                if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);
                File.AppendAllText(logFileName, logInfo + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: can not access log file " + logFileName + ".\n" + ex.Message);
            }
        }
    }
}