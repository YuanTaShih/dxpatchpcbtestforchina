﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DxPatchPCBTestApp
{
    public static class DriveHelper
    {
        private static List<DriveInfo> drives = new List<DriveInfo>();

        public static List<DriveInfo> getRemovableDrives()
        {
            List<DriveInfo> removableDrives = new List<DriveInfo>();

            foreach (DriveInfo drive in DriveInfo.GetDrives()){
                if (drive.IsReady && drive.DriveType == DriveType.Removable){
                    if (DriveHelper.ContainsRawFile(drive.RootDirectory.FullName)){
                        removableDrives.Add(drive);
                    }
                }
            }
    
            return removableDrives;
        }

        public static bool ContainsRawFile(string dir)
        {
            return GetRawFiles(dir).Length >= 1 ? true :false;
        }

        private static string[] GetRawFiles(string dir)
        {
            List<string> rawFiles = new List<string>();
            var files = Directory.GetFiles(dir, "*.*", SearchOption.TopDirectoryOnly);

            foreach (var f in files)
            {
                string ext = Path.GetExtension(f).Replace(".", "");
                Regex rexp = new Regex(@"\d\d\d");

                if (ext.ToUpper().Contains("RAW") || rexp.Match(ext).Success)
                {
                    rawFiles.Add(f);
                }
            }

            return rawFiles.ToArray();
        }

        private static string GetSingleRawFile(string[] rawFilePaths)
        {
            List<string> numberFiles = new List<string>();
            string singleRawFilePath = "";
            
            Regex numRegExp = new Regex(@"\d\d\d");

            foreach (var f in rawFilePaths)
            {
                string ext = Path.GetExtension(f).ToUpper().Replace(".", "");
                if (ext == "RAW") {
                    singleRawFilePath = f;
                } 
                else if (numRegExp.Match(ext).Success) {
                    numberFiles.Add(f);
                }
            }

            if(numberFiles.Count >= 1) 
            {
                int max = 0;

                foreach (var f in numberFiles)
                {
                    try {
                        int number = Int32.Parse(Path.GetExtension(f).ToUpper().Replace(".", ""));
                        if (number > max && File.Exists(f)) singleRawFilePath = f;
                    }
                    catch (Exception ex) {
                        AppLog.Write("Error parsing number RAW file: " + f + "\n" + ex.Message);
                    }
                }
            }

            return singleRawFilePath;
        }

        public static string[] GetRawFilesFromDrives()
        {
            const bool TEST_MODE = false;

            List<string> filePaths = new List<string>();
   
            foreach (DriveInfo drive in DriveInfo.GetDrives()){
                if(drive.IsReady && drive.DriveType == DriveType.Removable){
                    //Check if drive contains .RAW data file
                    filePaths.AddRange(DriveHelper.GetRawFiles(drive.RootDirectory.FullName));

                    //if (filePaths.Count >= 1){
                    //    //Found a RAW file in this drive
                    //    string rawFilePath = GetSingleRawFile(filePaths.ToArray());
                    //    filePaths.Clear();
                    //    filePaths.Add(rawFilePath);
                    //    break;
                    //}
                }
            }

            if (TEST_MODE && filePaths.Count <= 0) {
                string testdir = AppConfig.OutputDirectory + @"\usbtest\";
                try { filePaths.AddRange(Directory.GetFiles(testdir, "*.RAW")); }
                catch (DirectoryNotFoundException ex) {
                    Console.WriteLine("Directory not found: " + testdir);
                }
            }

            return filePaths.ToArray();
        }

        public static bool DrivesChanged()
        {
            bool changed = false;
            List<DriveInfo> drivesNow = DriveHelper.getRemovableDrives();
            
            //Check if a new removable drive has been found
            foreach(DriveInfo drive in drivesNow){
                
                bool found = false;

                foreach (DriveInfo oldDrive in DriveHelper.drives) {
                    if(drive.DriveType == oldDrive.DriveType 
                        && drive.Name == oldDrive.Name){
                        found = true;
                    }
                }
            
                if(found == false){
                    changed = true;
                    break;
                }
            }
            
            DriveHelper.drives = drivesNow;
            return changed;
        }

    }
}
