﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;

namespace DxPatchPCBTestApp
{
    public static class AppConfig
    {
        public static string DateTimeFormat { get; } = "yyyy-MM-dd hh:mm:ss.fff";
        public static int PatchSampleRate { get; } = 500;
        public static double PatchEcgResolution { get; } = 0.005;
        public static string PPIDPlaceholder { get; } = "CH XX ID XXXXX";
        public static string OutputDirectory { get; set; } = @"c:\temp";
        public static string SignalGeneratorScriptPath { get; set; } = @"c:\temp\sig-gen-script.xml";

        public static void LoadConfig()
        {
            string[] configItems = new string[] { "" };

            try { configItems = File.ReadAllLines( GetConfigFilePath() ); }
            catch (Exception ex) {
                Console.WriteLine("Error loading config file.\n" + ex.Message);
            }

            foreach (string line in configItems) {
                if (!line.Contains(":")) break;

                string[] pair = line.Split(new char[] { ':' }, 2, StringSplitOptions.None);
                string key = pair[0].Trim();
                string val = pair[1].Trim();

                if (pair.Length <= 1) continue;
                if (key.ToUpper() == "OUTPUT_DIRECTORY" && CreateDirIfNotExists(val)) {
                    OutputDirectory = val;
                }
                if (key.ToUpper() == "SIG_GEN_SCRIPT") {
                    SignalGeneratorScriptPath = val;
                }
            }
        }

        private static bool CreateDirIfNotExists(string dirPath)
        {
            if (!Directory.Exists(dirPath)) { 
                try { Directory.CreateDirectory(dirPath); }
                catch (Exception ex) {
                    MessageBox.Show("Error creating directory: " + dirPath + "\n" + ex.Message);
                }
            }
            return Directory.Exists(dirPath);
        }

        public static void SaveConfig() {

            string configText = "";
            configText += "OUTPUT_DIRECTORY: " + OutputDirectory + "\n";
            configText += "SIG_GEN_SCRIPT: " + SignalGeneratorScriptPath;

            try {
                File.WriteAllText( GetConfigFilePath(), configText);
            }
            catch (Exception ex){ MessageBox.Show("Error saving to config file.\n" + ex.Message); }
        }

        private static string GetConfigFileName(){
            return Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName) + ".config";
        }

        public static string GetConfigFilePath() {

            string configFilePath = "";

            try {
                string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string configDirPath = appDataDir + "\\ApoDx\\";
                configFilePath = configDirPath + GetConfigFileName();
                if (!Directory.Exists(configDirPath)) Directory.CreateDirectory(configDirPath);
                if (!File.Exists(configFilePath)) File.Create(configFilePath).Dispose();
            }
            catch (Exception ex) { Console.WriteLine("Error getting config file.\n" + ex.Message); }

            return configFilePath;
        }
    }
}
